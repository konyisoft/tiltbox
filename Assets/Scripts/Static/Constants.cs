namespace Konyisoft.Tilt
{
	public static class Constants
	{
		public static class Texts
		{
			public const string Moves = "<color=#b6b6b6>MOVES:</color> {0}";
			public const string Time = "<color=#b6b6b6>TIME:</color> {0}";
			public const string Level = "LEVEL {0}";
			public const string Unlimited = "--:--";

			public const string Success = "SUCCESS";
			public const string Failed = "FAILED";
			public const string TimeIsUp = "TIME IS UP";
			public const string TheEnd = "THE END";
			
			public const string TapToStart = "TAP TO START";
			public const string TapToRestart = "TAP TO RESTART";
			public const string TapToNextLevel = "TAP TO NEXT LEVEL";
			public const string ThanksForPlaying = "THANKS FOR PLAYING";

			public const string Yes = "Yes";
			public const string No = "No";
			public const string OK = "OK";
			public const string ConfirmRestart = "Do you really want to restart this level?";
			public const string ConfirmExit = "Do you really want to exit game?";
			public const string ConfirmResetAll = "Do you really want to reset all level results?";

			public static string[] AudioSettings = {
				"Music and sounds",
				"Music only",
				"Sounds only",
				"Silence"
			};

			public const string Enabled = "Enabled";
			public const string Disabled = "Disabled";
		}

		public static class ObjectNames
		{
			public const string ItemRed     = "CubeRed";
			public const string ItemGreen   = "CubeGreen";
			public const string ItemBlue    = "CubeBlue";
			public const string ItemMagenta = "CubeMagenta";
			public const string ItemOrange  = "CubeOrange";
			public const string ItemYellow  = "CubeYellow";
			public const string ItemWhite   = "CubeWhite";

			public const string SlotRed     = "SlotRed";
			public const string SlotGreen   = "SlotGreen";
			public const string SlotBlue    = "SlotBlue";
			public const string SlotMagenta = "SlotMagenta";
			public const string SlotOrange  = "SlotOrange";
			public const string SlotYellow  = "SlotYellow";
			public const string SlotWhite   = "SlotWhite";
		}

		public static class Audio
		{
			public const string Click  = "Click";
		}
	}
}
