using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class ObjectPool : GameBehaviour
	{
		#region Fields
		
		PooledObject prefab;
		List<PooledObject> availableObjects = new List<PooledObject>();
		
		#endregion
		
		#region Public methods

		public PooledObject GetObject()
		{
			PooledObject obj;
			int lastAvailableIndex = availableObjects.Count - 1;
			if (lastAvailableIndex >= 0)
			{
				obj = availableObjects[lastAvailableIndex];
				availableObjects.RemoveAt(lastAvailableIndex);
				obj.gameObject.SetActive(true);
			}
			else
			{
				obj = Instantiate<PooledObject>(prefab);
				obj.transform.SetParent(transform, false);
				obj.Pool = this;
			}
			return obj;
		}

		public void AddObject(PooledObject obj)
		{
			obj.gameObject.SetActive(false);
			obj.transform.SetParent(transform, false);
			availableObjects.Add(obj);
		}		
		
		public static ObjectPool GetPool(PooledObject prefab)
		{
			GameObject obj;
			ObjectPool pool;
			if (Application.isEditor)
			{
				obj = GameObject.Find(prefab.name + " Pool");
				if (obj != null)
				{
					pool = obj.GetComponent<ObjectPool>();
					if (pool != null)
					{
						return pool;
					}
				}
			}
			obj = new GameObject(prefab.name + " Pool");
			pool = obj.AddComponent<ObjectPool>();
			pool.prefab = prefab;
			pool.transform.SetParent(PoolManager.Instance.poolsParent);
			return pool;
		}
		
		#endregion		
	}
}
