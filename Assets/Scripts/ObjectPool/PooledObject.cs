using System;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class PooledObject : GameBehaviour
	{
		#region Fields
		
		[NonSerialized]
		ObjectPool poolInstanceForPrefab;
		
		#endregion
		
		#region Properties

		public ObjectPool Pool
		{
			get; set;
		}
		
		#endregion
		
		#region Public methods
		
		public void ReturnToPool()
		{
			if (Pool != null)
			{
				Pool.AddObject(this);
			}
			else
			{
				Destroy(gameObject);
			}
			OnDeactivated();
		}
		
		public T GetPooledInstance<T>() where T : PooledObject
		{
			if (poolInstanceForPrefab == null)
			{
				poolInstanceForPrefab = ObjectPool.GetPool(this);
			}
			OnActivated();
			return (T)poolInstanceForPrefab.GetObject();
		}
		
		#endregion

		#region Protected methods

		protected virtual void OnActivated()
		{
		}

		protected virtual void OnInitialized()
		{
		}

		protected virtual void OnDeactivated()
		{
		}

		#endregion
	}
}
