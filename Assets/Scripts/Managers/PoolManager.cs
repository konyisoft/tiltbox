using UnityEngine;

namespace Konyisoft.Tilt
{
	public class PoolManager : Singleton<PoolManager>
	{
		#region Fields

		public Transform poolsParent;
		
		#endregion
		
		#region Mono methods
		
		void Awake()
		{
			if (poolsParent == null)
			{
				poolsParent = transform; // this
			}
		}
		
		#endregion
	}
}
