using System;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class LevelManager : Singleton<LevelManager>
	{
		#region Fields

		public Transform levelParent;
		public float levelRotationSpeed = 10f;
		public float maxLevelAngle = 15f;
		public float objectSize = 1f;
		public Vector3 itemsOffset;
		public Vector3 tilesOffset;
		public Level[] levels = new Level[0];

		int levelIndex = -1;
		Level level;
		Vector3 desiredEulerAngles;
		Vector3 currentEulerAngles;
		LevelItemData[] items;
		LevelTileData[] tiles;

		#endregion

		#region Properties

		public int LevelCount
		{
			get { return levels.Length; }
		}

		public int LevelIndex
		{
			get { return levelIndex; }
		}

		public bool IsFinalLevel
		{
			get { return levelIndex == LevelCount - 1 ;}
		}

		public int DisplayedLevelIndex
		{
			// Returns levelIndex + 1
			get { return levelIndex > -1 ? levelIndex + 1 : -1; }
		}

		public bool HasLevelLoaded
		{
			get { return level != null; }
		}

		public Level Level
		{
			get { return level; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			if (levelParent == null)
			{
				Debug.LogError("No level parent object attached.", this);
				return;
			}

			if (levels.Length < 1)
			{
				Debug.LogError("No level prefabs attached.", this);
				return;
			}

			// Shortcuts to prefab data
			items = ItemDataContainer.Instance.items;
			tiles = TileDataContainer.Instance.tiles;
		}

		void Update()
		{
			if (levelParent == null)
				return;

			currentEulerAngles = Vector3.Lerp(currentEulerAngles, desiredEulerAngles, Time.deltaTime * levelRotationSpeed);
			levelParent.localRotation = Quaternion.Euler(currentEulerAngles);
		}

		#endregion

		#region Public methods

		public void CreateLevel(int levelIndex)
		{
			// Destroy current level (if exists)
			ClearLevel();

			if (levelParent != null)
			{
				if (levelIndex < levels.Length)
				{
					// Get and instantiate level prefab
					Level prefab = levels[levelIndex];
					if (prefab != null)
					{
						level = Instantiate(prefab) as Level;
						level.transform.SetParent(transform);
						level.gameObject.name = level.gameObject.name.RemoveCloneSuffix();
						this.levelIndex = levelIndex;
					}
					else
					{
						Debug.LogError("Level prefab with index #" + levelIndex + " is null", this);
						return;
					}
				}
				else
				{
					Debug.LogError("Level not found with index #" + levelIndex + ".", this);
					return;
				}
			}
		}

		public void ClearLevel()
		{
			if (level != null)
			{
				level.Clear();
				Destroy(level.gameObject);
			}
			levelIndex = -1;
			level = null;
		}

		public LevelItemData GetLevelItemData(Color32 color)
		{
			for (int i = 0; i < items.Length; i++)
			{
				if (items[i].color.Compare(color))
				{
					return items[i];
				}
			}
			return null;
		}

		public LevelTileData GetLevelTileData(Color32 color)
		{
			for (int i = 0; i < tiles.Length; i++)
			{
				if (tiles[i].color.Compare(color))
				{
					return tiles[i];
				}
			}
			return null;
		}

		public void SetLevelRotation(float x, float y, float z)
		{
			desiredEulerAngles = new Vector3(x, y, z);
		}

		public void ResetLevelRotation()
		{
			SetLevelRotation(0, 0, 0);
		}

		public void MoveItems(Direction direction)
		{
			if (HasLevelLoaded)
			{
				level.MoveItems(direction);
			}
		}

		#endregion

		#region Public static methods

		public static Direction GetOppositeDirection(Direction direction)
		{
			Direction opposite = Direction.None;
			switch (direction)
			{
				case Direction.Forward:
					opposite = Direction.Back;
					break;
				case Direction.Back:
					opposite = Direction.Forward;
					break;
				case Direction.Left:
					opposite = Direction.Right;
					break;
				case Direction.Right:
					opposite = Direction.Left;
					break;
			}
			return opposite;
		}

		#endregion
	}
}
