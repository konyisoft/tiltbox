using UnityEngine;

namespace Konyisoft.Tilt
{
	public static class Options
	{
		#region Enumerations

		public enum AudioSetting
		{
			MusicAndSound = 0,
			MusicOnly     = 1,
			SoundOnly     = 2,
			None          = 3,
		}

		#endregion

		#region Fields

		public static float buttonScale;
		public static float buttonGap;
		public static AudioSetting audio;
		public static bool timeLimit;

		#endregion

		#region Properties

		public static bool Loaded
		{
			get; private set;
		}

		#endregion

		#region Constants

		public const float DefaultButtonScale = 1f;
		public const float DefaultButtonGap = 180f;
		public const AudioSetting DefaultAudio = AudioSetting.MusicAndSound;
		public const bool DefaultTimeLimit = true;

		#endregion

		#region Public methods

		public static void Load()
		{
			buttonScale = PlayerPrefs.GetFloat("ButtonScale", DefaultButtonScale);
			buttonGap = PlayerPrefs.GetFloat("ButtonGap", DefaultButtonGap);
			audio = (AudioSetting)PlayerPrefs.GetInt("Audio", 0);
			timeLimit = PlayerPrefs.GetInt("TimeLimit", 1) == 1 ? true : false;
			Loaded = true;
		}

		public static void Save()
		{
			PlayerPrefs.SetFloat("ButtonScale", buttonScale);
			PlayerPrefs.SetFloat("ButtonGap", buttonGap);
			PlayerPrefs.SetInt("Audio", (int)audio);
			PlayerPrefs.SetInt("TimeLimit", timeLimit ? 1 : 0);
		}

		public static void RestoreDefaults()
		{
			buttonScale = DefaultButtonScale;
			buttonGap = DefaultButtonGap;
			audio = DefaultAudio;
			timeLimit = DefaultTimeLimit;
			Save();
		}

		#endregion
	}
}

