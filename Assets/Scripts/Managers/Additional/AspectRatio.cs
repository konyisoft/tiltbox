using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public static class AspectRatio
	{
		const float ScreenX = 16.0f;
		const float ScreenY = 9.0f;

		public static float GetTargetAspectRatio()
		{
			// The desired aspect ratio
			return ScreenX / ScreenY;
		}

		public static float GetWindowAspectRatio()
		{
			// The game window's current aspect ratio
			return (float)Screen.width / (float)Screen.height;
		}

		public static float GetScaleHeight()
		{
			// Current viewport height should be scaled by this amount
			return GetWindowAspectRatio() / GetTargetAspectRatio();
		}

		public static void SetCamerasAspect(params Camera[] cameras)
		{
			if (cameras == null)
				return;

			for (int i = 0; i < cameras.Length; i++)
			{
				Camera camera = cameras[i];
				if (camera != null)
				{
					float scaleHeight = GetScaleHeight();

					// if scaled height is less than current height, add letterbox
					if (scaleHeight < 1.0f)
					{
						Rect rect = camera.rect;

						rect.width = 1.0f;
						rect.height = scaleHeight;
						rect.x = 0;
						rect.y = (1.0f - scaleHeight) / 2.0f;

						camera.rect = rect;
					}
					else // add pillarbox
					{
						float scaleWidth = 1.0f / scaleHeight;

						Rect rect = camera.rect;

						rect.width = scaleWidth;
						rect.height = 1.0f;
						rect.x = (1.0f - scaleWidth) / 2.0f;
						rect.y = 0;

						camera.rect = rect;
					}
				}
			}
		}
	}
}