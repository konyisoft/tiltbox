using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public static class LevelPrefs
	{
		#region Classes

		public class LevelPrefData
		{
			public string levelName;
			public int moves;
			public int time;

			public LevelPrefData(string levelName, int moves, int time)
			{
				this.levelName = levelName;
				this.moves = moves;
				this.time = time;
			}
		}

		#endregion

		#region Fields

		static List<LevelPrefData> prefData = new List<LevelPrefData>();

		#endregion

		#region Properties

		public static List<LevelPrefData> PrefData
		{
			get { return prefData; }
		}

		public static bool Loaded
		{
			get; private set;
		}

		#endregion

		#region Constants

		const string PrefKey = "LevelPrefs";

		#endregion

		#region Public methods

		public static void Load()
		{
			prefData.Clear();
			// Load data if key exists
			if (PlayerPrefs.HasKey(PrefKey))
			{
				prefData = StringToData(PlayerPrefs.GetString(PrefKey));
			}
			// Otherwise create an empty list and save that (creates key)
			else
			{
				prefData = new List<LevelPrefData>(); // empty list
				Save();
			}
			Loaded = true;
		}

		public static void Save()
		{
			PlayerPrefs.SetString(PrefKey, DataToString(prefData));
			PlayerPrefs.Save();
		}

		public static void Clear()
		{
			prefData.Clear();
			PlayerPrefs.SetString(PrefKey, string.Empty);
			PlayerPrefs.Save();
		}

		public static void AddOrModifyData(string levelName, int moves, int time)
		{
			if (!Loaded)
				return;

			// Try to find existing data
			LevelPrefData data = prefData.Find(d => d.levelName == levelName);
			
			// Modify
			if (data != null)
			{
				// Modify only if "moves" is smaller than the stored value
				// Or moves are equal but the time smaller
				if (moves < data.moves || (moves == data.moves && time < data.time))
				{
					data.moves = moves;
					data.time = time;
					Save();
				}
			}
			// Create new
			else
			{
				data = new LevelPrefData(levelName, moves, time);
				prefData.Add(data);
				Save();
			}
		}

		public static LevelPrefData GetData(string levelName)
		{
			return prefData.Find(d => d.levelName == levelName);
		}

		#endregion

		#region Private methods

		static string DataToString(List<LevelPrefData> prefData)
		{
			string s = string.Empty;
			for (int i = 0; i < prefData.Count; i++)
			{
				LevelPrefData data = prefData[i];
				s += string.Format("{0},{1},{2}|", data.levelName, data.moves, data.time);
			}
			s = s.TrimEnd('|'); // Remove the last separator
			return s;
		}

		static List<LevelPrefData> StringToData(string s)
		{
			List<LevelPrefData> prefData = new List<LevelPrefData>();
			string[] rows = s.Split('|');  // split string to data items
			for (int i = 0; i < rows.Length; i++)
			{
				// Trying to parse item
				try
				{
					string[] strings = rows[i].Split(',');
					if (strings.Length == 3)
					{
						LevelPrefData data = new LevelPrefData(
							strings[0],
							int.Parse(strings[1]),
							int.Parse(strings[2])
						);
						prefData.Add(data);
					}
				}
				catch
				{
					// Nothing to do in this case
				}
			}
			return prefData;
		}

		#endregion
	}
}