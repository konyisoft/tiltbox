using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class PlayerManager : Singleton<PlayerManager>
	{
		#region Fields

		public float actionTime = 1f;  // Time for the actions (rolling, sliding etc.) take to finish

		string currentKeyName = string.Empty;
		Direction currentDirection;
		bool isActionRunning;

		#endregion

		#region Constants

		const string KEY_FORWARD = "Forward";
		const string KEY_BACK    = "Back";
		const string KEY_LEFT    = "Left";
		const string KEY_RIGHT   = "Right";

		#endregion

		#region Mono methods

		void Update()
		{
			// Paused?
			if (GameManager.Instance.Paused)
				return;

			// Exit? (on all platforms)
			if (Input.GetKeyUp(KeyCode.Escape))
			{
				if (GameManager.Instance.GameState == GameState.Title)
				{
					// Options visible? Go back to title
					if (UIManager.Instance.optionsPanel.Visible)
					{
						UIManager.Instance.SaveAndHideOptions();
					}
					// Quit
					else
					{
						GameManager.Instance.Quit();
					}
				}
				else
				{
					UIManager.Instance.ShowDialog(
						Constants.Texts.ConfirmExit,
						new string[] { Constants.Texts.Yes, Constants.Texts.No },
						OnExitDialog
					);
				}
			}

			// No level loaded yet
			if (!LevelManager.Instance.HasLevelLoaded)
				return;

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
			// Keyboard on desktop platforms only
			HandleKeyboard();
#endif
			// Handled on all platforms
			HandleTouch();
		}

		void OnDestroy()
		{
			StopAllCoroutines();
		}

		#endregion

		#region Public methods

		public void ResetInput()
		{
			currentKeyName = string.Empty;
			currentDirection = Direction.None;
		}

		#endregion

		#region Private methods

		void DoAction(Direction direction)
		{
			StopAllCoroutines();
			StartCoroutine(CO_DoAction(direction));
		}

		IEnumerator CO_DoAction(Direction direction)
		{
			isActionRunning = true;
			LevelManager.Instance.MoveItems(direction);
			yield return new WaitForSeconds(actionTime);
			// Wait for all the other coroutines to finish (which use actionTime)
			yield return new WaitForEndOfFrame();
			isActionRunning = false;
		}

		void RotateLevel(Direction direction)
		{
			float maxAngle = LevelManager.Instance.maxLevelAngle;
			switch (direction)
			{
				case Direction.Forward:
					LevelManager.Instance.SetLevelRotation(maxAngle, 0, 0);
					break;
				case Direction.Back:
					LevelManager.Instance.SetLevelRotation(-maxAngle, 0, 0);
					break;
				case Direction.Left:
					LevelManager.Instance.SetLevelRotation(0, 0, maxAngle);
					break;
				case Direction.Right:
					LevelManager.Instance.SetLevelRotation(0, 0, -maxAngle);
					break;
			}
			UIManager.instance.MoveBackground(direction);
		}

		void HandleKeyboard()
		{
			// Not in loaded or playing state
			if (GameManager.Instance.GameState != GameState.LevelLoaded &&
				GameManager.Instance.GameState != GameState.Playing)
				return;

			// Control keys down
			if (Input.GetButtonDown(KEY_FORWARD))
			{
				currentKeyName = KEY_FORWARD;
				currentDirection = Direction.Forward;
				RotateLevel(currentDirection);
			}

			if (Input.GetButtonDown(KEY_BACK))
			{
				currentKeyName = KEY_BACK;
				currentDirection = Direction.Back;
				RotateLevel(currentDirection);
			}

			if (Input.GetButtonDown(KEY_LEFT))
			{
				currentKeyName = KEY_LEFT;
				currentDirection = Direction.Left;
				RotateLevel(currentDirection);
			}

			if (Input.GetButtonDown(KEY_RIGHT))
			{
				currentKeyName = KEY_RIGHT;
				currentDirection = Direction.Right;
				RotateLevel(currentDirection);
			}

			// Any key pressed?
			if (!string.IsNullOrEmpty(currentKeyName) && currentDirection != Direction.None)
			{
				// Start to play on first key down
				if (GameManager.Instance.GameState == GameState.LevelLoaded)
				{
					GameManager.Instance.Play();
				}

				// Current key pressed continuously
				if (!isActionRunning && Input.GetButton(currentKeyName))
				{
					DoAction(currentDirection);
				}
				// Current key up
				else if (!string.IsNullOrEmpty(currentKeyName) && Input.GetButtonUp(currentKeyName))
				{
					LevelManager.Instance.ResetLevelRotation();
					UIManager.Instance.ResetBackground();
					ResetInput();
				}
			}
		}

		void HandleTouch()
		{
			// Not in loaded or playing state
			if (GameManager.Instance.GameState != GameState.LevelLoaded &&
				GameManager.Instance.GameState != GameState.Playing)
				return;

			// Current button pressed continuously
			if (!isActionRunning && currentDirection != Direction.None)
			{
				DoAction(currentDirection);
			}
		}

		#endregion

		#region Event handlers

		public void OnPointerDown(string buttonID)
		{
			// No level loaded yet
			if (!LevelManager.Instance.HasLevelLoaded)
				return;

			// Paused?
			if (GameManager.Instance.Paused)
				return;

			// Not in loaded or playing state
			if (GameManager.Instance.GameState != GameState.LevelLoaded &&
				GameManager.Instance.GameState != GameState.Playing)
				return;

			// Start to play on first button down
			if (GameManager.Instance.GameState == GameState.LevelLoaded)
			{
				GameManager.Instance.Play();
			}

			// Button down
			switch (buttonID)
			{
				case "forward":
					currentDirection = Direction.Forward;
					RotateLevel(currentDirection);
					break;
				case "back":
					currentDirection = Direction.Back;
					RotateLevel(currentDirection);
					break;
				case "left":
					currentDirection = Direction.Left;
					RotateLevel(currentDirection);
					break;
				case "right":
					currentDirection = Direction.Right;
					RotateLevel(currentDirection);
					break;
			}
		}

		public void OnPointerUp(string buttonID)
		{
			// Button up
			if (currentDirection != Direction.None)
			{
				LevelManager.Instance.ResetLevelRotation();
				UIManager.Instance.ResetBackground();
				ResetInput();
			}
		}

		void OnExitDialog(int buttonIndex)
		{
			if (buttonIndex == 0) // Yes
			{
				// Go to the title
				GameManager.Instance.TitleScreen();
			}
		}

		#endregion
	}
}
