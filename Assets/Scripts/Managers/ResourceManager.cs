using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class ResourceManager : Singleton<ResourceManager>
	{
		#region Fields

		Dictionary<string, UnityEngine.Object> cache = new Dictionary<string, UnityEngine.Object>();

		#endregion

		#region Public methods

		public T Get<T>(string name) where T : UnityEngine.Object
		{
			UnityEngine.Object o = null;

			// Retrieve from cache
			if (cache.ContainsKey(name))
			{
				o = cache[name];
			}
			// Load from resource and add to cache
			else
			{
				o = Resources.Load(name);
				cache.Add(name, o);
			}

			return (T)System.Convert.ChangeType(o, typeof(T));
		}

		public Texture2D GetTexture2D(string name)
		{
			return Get<Texture2D>(name);
		}
		
		public GameObject GetPrefab(string name)
		{
			return Get<GameObject>(name);
		}

		#endregion
	}
}
