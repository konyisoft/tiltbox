using UnityEngine;

namespace Konyisoft.Tilt
{
	public class DisplayManager : Singleton<DisplayManager>
	{
		#region Fields

		public Camera mainCamera;

		float orthographicSize;

		#endregion

		#region Mono methods

		void Awake()
		{
			// Prevent sleep
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			if (mainCamera == null)
			{
				Debug.LogWarning("No main camera attached.");
			}
			else
			{
				orthographicSize = mainCamera.orthographicSize; // store orig value
			}

			// Don't need this at this time
			//AspectRatio.SetCamerasAspect(mainCamera);
		}

		#endregion

		#region Public methods

		public void SetCameraOrthographicSize(float additional)
		{
			// Adds additional value to the orginal one
			if (mainCamera != null)
			{
				mainCamera.orthographicSize = orthographicSize + additional;
			}
		}

		public void ResetCameraOrthographicSize()
		{
			SetCameraOrthographicSize(0);
		}

		#endregion
	}
}
