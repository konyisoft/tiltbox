using System;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIManager : Singleton<UIManager>
	{
		#region Fields

		public UIBackground background;
		public UIButtonPanel buttonPanel;
		public UICaptionPanel captionPanel;
		public UIDescriptionPanel descriptionPanel;
		public UIDialogPanel dialogPanel;
		public UIGamePanel gamePanel;
		public UIOptionsPanel optionsPanel;
		public UITitlePanel titlePanel;

		public TextGradient.GradientColors normalGradient;
		public TextGradient.GradientColors successGradient;
		public TextGradient.GradientColors failedGradient;

		#endregion

		#region Mono methdos

		void Awake()
		{
			if (dialogPanel != null && dialogPanel.Visible)
			{
				dialogPanel.Hide();
			}

			if (optionsPanel != null && optionsPanel.Visible)
			{
				optionsPanel.Hide();
			}

			if (background != null && !background.gameObject.activeInHierarchy)
			{
				background.gameObject.SetActive(true);
			}
		}

		#endregion

		#region Public methods

		public void MoveBackground(Direction direction)
		{
			if (background != null)
			{
				background.Move(direction);
			}
		}

		public void ResetBackground()
		{
			if (background != null)
			{
				background.ResetMove();
			}
		}

		public void SetBackgroundSprite(Sprite sprite)
		{
			if (background != null)
			{
				background.SetSprite(sprite);
			}
		}

		public void ShowButtons()
		{
			if (buttonPanel != null && !buttonPanel.Visible)
			{
				buttonPanel.Show();
			}
		}

		public void HideButtons()
		{
			if (buttonPanel != null && buttonPanel.Visible)
			{
				buttonPanel.Hide();
			}
		}

		public void ShowCaption(string text, string subtext, bool immediate = false)
		{
			if (captionPanel != null && !string.IsNullOrEmpty(text))
			{
				captionPanel.Show(text, subtext, immediate);
			}
		}

		public void HideCaption(bool immediate = false)
		{
			if (captionPanel != null)
			{
				captionPanel.Hide(immediate);
			}
		}

		public void SetCaptionAutohide(bool autohide)
		{
			if (captionPanel != null)
			{
				captionPanel.autohide = autohide;
			}
		}

		public void SetCaptionGradientColors(TextGradient.GradientColors gradientColors)
		{
			if (captionPanel != null)
			{
				captionPanel.SetTextGradientColors(gradientColors);
			}
		}

		public void ShowDescription(string text, bool immediate = false)
		{
			if (descriptionPanel != null && !string.IsNullOrEmpty(text))
			{
				descriptionPanel.Show(text, immediate);
			}
		}

		public void HideDescription(bool immediate = false)
		{
			if (descriptionPanel != null)
			{
				descriptionPanel.Hide(immediate);
			}
		}

		public void SubscribeDescriptionAutohide(UIDescriptionPanel.OnAutohide func)
		{
			if (descriptionPanel != null)
			{
				descriptionPanel.onAutoHide += func;
			}
		}

		public void UnsubscribeDescriptionAutohide(UIDescriptionPanel.OnAutohide func)
		{
			if (descriptionPanel != null)
			{
				descriptionPanel.onAutoHide -= func;
			}
		}

		public void ShowDialog(string text, string[] buttonCaptions, Action<int> callback)
		{
			if (dialogPanel != null && !dialogPanel.Visible)
			{
				dialogPanel.Show(text, buttonCaptions, callback);
			}
		}

		public void HideDialog()
		{
			if (dialogPanel != null && dialogPanel.Visible)
			{
				dialogPanel.Hide();
			}
		}

		public void SetMoveCounter(int value)
		{
			if (gamePanel != null)
			{
				gamePanel.SetMoveCounterText(string.Format(Constants.Texts.Moves, value));
			}
		}

		public void SetTimer(int value)
		{
			if (gamePanel != null)
			{
				string timeString = Timer.GetTimeString(value);
				gamePanel.SetTimerText(string.Format(Constants.Texts.Time, timeString));
			}
		}

		public void SetTimerDisabled()
		{
			if (gamePanel != null)
			{
				gamePanel.SetTimerText(string.Format(Constants.Texts.Time, Constants.Texts.Unlimited));
			}
		}

		public void ShowGamePanel(bool immediate = false)
		{
			if (gamePanel != null && !gamePanel.Visible)
			{
				gamePanel.Show(immediate);
			}
		}

		public void HideGamePanel(bool immediate = false)
		{
			if (gamePanel != null && gamePanel.Visible)
			{
				gamePanel.Hide(immediate);
			}
		}

		public void ShowOptions()
		{
			if (optionsPanel != null)
			{
				optionsPanel.Show();
			}
		}

		public void HideOptions()
		{
			if (optionsPanel != null)
			{
				optionsPanel.Hide();
			}
		}

		public void SaveAndHideOptions()
		{
			if (optionsPanel != null)
			{
				optionsPanel.OnButtonBackClick();
			}
		}

		public void ShowTitle()
		{
			if (titlePanel != null)
			{
				titlePanel.Show();
				SetBackgroundSprite(titlePanel.background);
			}
		}

		public void HideTitle()
		{
			if (titlePanel != null)
			{
				titlePanel.Hide();
			}
		}

		#endregion
	}
}
