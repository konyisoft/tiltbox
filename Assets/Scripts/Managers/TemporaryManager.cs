using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class TemporaryManager : Singleton<TemporaryManager>
	{
		#region Fields

		List<GameObject> objects = new List<GameObject>();

		#endregion

		#region Public methods

		public void Add(GameObject go)
		{
			if (go != null && !objects.Contains(go))
			{
				objects.Add(go);
				go.transform.SetParent(this.transform);
			}
		}

		public void Add(Component component)
		{
			if (component != null)
			{
				Add(component.gameObject);
			}
		}

		public void Remove(GameObject go, bool destroyImmediate = false)
		{
			if (go != null && objects.Contains(go))
			{
				objects.Remove(go);
				if (destroyImmediate)
				{
					DestroyImmediate(go);
				}
				else
				{
					Destroy(go);
				}
			}
		}

		public void Remove(Component component, bool destroyImmediate = false)
		{
			if (component != null)
				Remove(component.gameObject, destroyImmediate);
		}

		public void Clear()
		{
			objects.ForEach(obj =>
			{
				if (obj != null)
					Destroy(obj);
			});
			objects.Clear();
		}
		
		public void Cleanup()
		{
			objects.RemoveAll(obj => obj == null);
		}

		#endregion
	}
}
