using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class DebugManager : Singleton<DebugManager>
	{
		#region Fields

		public bool debugToolsEnabled = false;
		public int startLevelIndex;
		public RectTransform debugPanel;
		public Text fpsText;

		public Canvas UICanvas;
		public Canvas UIBackgroundCanvas;

		public float updateInterval = 0.5f;
		[Range(1, 100)]
		public int targetFrameRate = 60;

		float accum = 0f;
		int frames = 0;
		float timeLeft = 0f;
		float fps = 0f;
		float lastSample = 0f;

		#endregion

		#region Properties

		public float FPS
		{
			get { return fps; }
		}

		public string FPSAsString
		{
			get { return fps.ToString("f2"); }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			// Debug UI visible?
			if (debugPanel != null)
			{
				debugPanel.gameObject.SetActive(debugToolsEnabled);
				gameObject.SetActive(debugToolsEnabled); // this object
			}
			
			// FPS timer start values
			timeLeft = updateInterval;
			lastSample = Time.realtimeSinceStartup;
		}

		void Start()
		{
			// if (debugToolsEnabled)
			// {
			// 	GameManager.Instance.NewGame(startLevelIndex);
			// }
		}

		void Update()
		{
			if (!debugToolsEnabled)
				return;

			// Measuring FPS
			frames++;
			float newSample = Time.realtimeSinceStartup;
			float deltaTime = newSample - lastSample;
			lastSample = newSample;

			timeLeft -= deltaTime;
			accum += 1.0f / deltaTime;

			if (timeLeft <= 0f)
			{
				fps = accum / frames;
				timeLeft = updateInterval;
				accum = 0f;
				frames = 0;
			}

			if (fpsText != null)
			{
				fpsText.text = FPSAsString;
			}

#if UNITY_EDITOR
			// Screenshot with no UI
			if (Input.GetKeyUp(KeyCode.C) && LevelManager.Instance.HasLevelLoaded)
			{
				StartCoroutine(CO_CaptureScreenshot());
			}
			// Screenshot with UI
			else if (Input.GetKeyUp(KeyCode.V) && LevelManager.Instance.HasLevelLoaded)
			{
				string fileName = "Screenshot_{0}.png";
				ScreenCapture.CaptureScreenshot(string.Format(fileName, LevelManager.Instance.Level.Name), 0);
			}
#endif
		}

		#endregion

		#region Private methods

#if UNITY_EDITOR
		IEnumerator CO_CaptureScreenshot()
		{
			SetUICanvasEnabled(false);
			SetUIBackgroundCanvasEnabled(false);

			yield return new WaitForEndOfFrame();

			string fileName = "Screenshot_{0}.png";
			ScreenCapture.CaptureScreenshot(string.Format(fileName, LevelManager.Instance.Level.Name), 0);
			
			SetUICanvasEnabled(true);
			SetUIBackgroundCanvasEnabled(true);
		}

		void SetUICanvasEnabled(bool enabled)
		{
			if (UICanvas != null)
			{
				UICanvas.enabled = enabled;
			}
		}

		void SetUIBackgroundCanvasEnabled(bool enabled)
		{
			if (UIBackgroundCanvas != null)
			{
				UIBackgroundCanvas.enabled = enabled;
			}
		}
#endif	

		#endregion

		#region Event handlers

		public void OnPreviousLevelClick()
		{
			GameManager.Instance.PreviousLevel();
		}

		public void OnNextLevelClick()
		{
			GameManager.Instance.NextLevel();
		}

		#endregion
	}
}
