using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class GameManager : Singleton<GameManager>
	{
		#region Fields

		public Timer timer;

		GameState gameState = GameState.None;
		PlayResult playResult = PlayResult.None;
		int moveCounter;
		bool paused;

		#endregion

		#region Properties

		public int MoveCounter
		{
			get { return moveCounter; }
		}

		public GameState GameState
		{
			get { return gameState; }
		}

		public PlayResult PlayResult
		{
			get { return playResult; }
		}

		public bool Paused
		{
			get { return paused; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			if (timer == null)
			{
				Debug.LogError("No timer object attached", this);
				return;
			}

			timer.onTimeSet += OnTimeSet;
			timer.onTimeTick += OnTimeTick;
			timer.onTimeElapsed += OnTimeElapsed;

			Options.Load();
			LevelPrefs.Load();
		}

		void Start()
		{
			TitleScreen();
		}

		#endregion

		#region Public methods

		public void TitleScreen()
		{
			UnloadLevel(); // if previously loaded
			UIManager.Instance.UnsubscribeDescriptionAutohide(OnDescriptionAutohide);
			UIManager.Instance.HideOptions();
			UIManager.Instance.HideButtons();
			UIManager.Instance.HideCaption(true);
			UIManager.Instance.HideDescription(true);
			UIManager.Instance.HideGamePanel(true);
			UIManager.Instance.ShowTitle();
			SetGameState(GameState.Title);
		}

		public void NewGame(int levelIndex = 0)
		{
			UIManager.Instance.HideTitle();
			UIManager.Instance.ShowButtons();
			LoadLevel(levelIndex);
		}

		public void Play()
		{
			if (Options.timeLimit)
			{
				timer.Run();
			}
			UIManager.Instance.UnsubscribeDescriptionAutohide(OnDescriptionAutohide);
			UIManager.Instance.HideDescription(false);
			UIManager.Instance.HideCaption(false);
			UIManager.Instance.ShowGamePanel(false);
			SetGameState(GameState.Playing);
		}

		public void Stop(bool resetLevelRotation = true)
		{
			if (Options.timeLimit)
			{
				timer.Pause();
			}
			PlayerManager.Instance.ResetInput();
			if (resetLevelRotation)
			{
				LevelManager.Instance.ResetLevelRotation();
			}
			SetGameState(GameState.Stopped);
		}

		public void Pause()
		{
			if (gameState == GameState.Playing && Options.timeLimit)
			{
				timer.Pause();
			}
			Time.timeScale = 0;
			paused = true;
		}

		public void Resume()
		{
			if (gameState == GameState.Playing)
			{
				timer.Resume();
			}
			Time.timeScale = 1f;
			paused = false;
		}

		public void Wait(float delay, Action before, Action after)
		{
			SetGameState(GameState.Waiting);
			StartCoroutine(CO_Wait(delay, before, after));
		}

		public void LoadLevel(int levelIndex, bool restarted = false)
		{
			Stop(true);
			SetPlayResult(PlayResult.None);
			LevelManager.Instance.CreateLevel(levelIndex);
			if (LevelManager.Instance.HasLevelLoaded)
			{
				LevelManager.Instance.Level.onAfterLevelRefresh += OnAfterLevelRefresh;
				moveCounter = 0;
				timer.Set(LevelManager.Instance.Level.time);
				UIManager.Instance.SetMoveCounter(moveCounter);

				// Restart current level or start a newly loaded one?
				if (restarted)
				{
					UIManager.Instance.ShowGamePanel(true);
					UIManager.Instance.HideDescription(true);
					UIManager.Instance.HideCaption(false);
					Play();
				}
				else
				{
					UIManager.Instance.HideGamePanel(true);
					UIManager.Instance.ShowDescription(LevelManager.Instance.Level.description, false);
					UIManager.Instance.SetCaptionAutohide(true);
					UIManager.Instance.SetCaptionGradientColors(UIManager.Instance.normalGradient);
					UIManager.Instance.ShowCaption(
						string.Format(Constants.Texts.Level, LevelManager.Instance.DisplayedLevelIndex),
						Constants.Texts.TapToStart,
						false
					);
					UIManager.Instance.SubscribeDescriptionAutohide(OnDescriptionAutohide);
					UIManager.Instance.SetBackgroundSprite(LevelManager.Instance.Level.background);
					SetGameState(GameState.LevelLoaded);
				}

				DisplayManager.Instance.SetCameraOrthographicSize(LevelManager.Instance.Level.cameraSizeAddition);
			}
		}

		public void UnloadLevel()
		{
			Stop(true);
			SetPlayResult(PlayResult.None);
			if (LevelManager.Instance.HasLevelLoaded)
			{
				LevelManager.Instance.ClearLevel();
				moveCounter = 0;
				timer.Set(0);
				UIManager.Instance.SetMoveCounter(moveCounter);
				UIManager.Instance.HideGamePanel(true);
				UIManager.Instance.HideDescription(true);
				UIManager.Instance.HideCaption(true);
				DisplayManager.Instance.ResetCameraOrthographicSize();
				SetGameState(GameState.None);
			}
		}

		public void RestartLevel()
		{
			// Reloads the currently loaded level
			if (LevelManager.Instance.HasLevelLoaded)
			{
				LoadLevel(LevelManager.Instance.LevelIndex, true);
			}
		}

		public void PreviousLevel()
		{
			if (LevelManager.Instance.HasLevelLoaded)
			{
				int levelIndex = LevelManager.Instance.LevelIndex;
				if (levelIndex > 0)
				{
					LoadLevel(levelIndex - 1);
				}
			}
		}

		public void NextLevel()
		{
			if (LevelManager.Instance.HasLevelLoaded)
			{
				int levelIndex = LevelManager.Instance.LevelIndex;
				if (levelIndex < LevelManager.Instance.LevelCount - 1)
				{
					LoadLevel(levelIndex + 1);
				}
			}
		}

		public void Quit()
		{
			Application.Quit();
		}

		#endregion

		#region Private methods

		void LevelCompleted()
		{
			Stop(true);
			SetPlayResult(PlayResult.Success);

			// Save results only if time limit enabled
			if (Options.timeLimit)
			{
				LevelPrefs.AddOrModifyData(
					LevelManager.Instance.Level.Name,
					MoveCounter,
					timer.ElapsedTime
				);
			}

			UIManager.Instance.SetCaptionAutohide(false);
			UIManager.Instance.SetCaptionGradientColors(UIManager.Instance.successGradient);
			if (LevelManager.Instance.IsFinalLevel)
			{
				UIManager.Instance.ShowCaption(Constants.Texts.TheEnd, Constants.Texts.ThanksForPlaying, false);
			}
			else
			{
				UIManager.Instance.ShowCaption(Constants.Texts.Success, Constants.Texts.TapToNextLevel, false);
			}
		}

		void LevelFailed()
		{
			Stop(false);
			SetPlayResult(PlayResult.Failed);

			UIManager.Instance.SetCaptionAutohide(false);
			UIManager.Instance.SetCaptionGradientColors(UIManager.Instance.failedGradient);
			UIManager.Instance.ShowCaption(Constants.Texts.Failed, Constants.Texts.TapToRestart, false);
		}

		void SetGameState(GameState gameState)
		{
			this.gameState = gameState;
		}

		void SetPlayResult(PlayResult playResult)
		{
			this.playResult = playResult;
		}

		IEnumerator CO_Wait(float delay, Action before, Action after)
		{
			if (before != null)
			{
				before();
			}

			yield return new WaitForSeconds(delay);
			
			if (after != null)
			{
				after();
			}
		}

		#endregion

		#region Event handlers

		void OnAfterLevelRefresh(MovementResult movementResult)
		{
			// NOTE: Order is important here
			if ((movementResult & MovementResult.Failed) == MovementResult.Failed)
			{
				LevelFailed();
				return;
			}

			if ((movementResult & MovementResult.Moved) == MovementResult.Moved)
			{
				moveCounter++;
				UIManager.Instance.SetMoveCounter(moveCounter);

				// Check fail
				if (LevelManager.Instance.Level.IsFailed)
				{
					LevelFailed();
				}
				// Check level completed
				else if (LevelManager.Instance.Level.IsCompleted)
				{
					LevelCompleted();
				}
			}
		}

		void OnTimeSet()
		{
			if (Options.timeLimit)
			{
				UIManager.Instance.SetTimer(timer.Time);
			}
			else
			{
				UIManager.Instance.SetTimerDisabled();
			}
		}

		void OnTimeTick()
		{
			UIManager.Instance.SetTimer(timer.Time);
		}

		void OnTimeElapsed()
		{
			UIManager.Instance.SetTimer(timer.Time);
			Stop(false);
			SetPlayResult(PlayResult.Failed);

			UIManager.Instance.SetCaptionAutohide(false);
			UIManager.Instance.SetCaptionGradientColors(UIManager.Instance.failedGradient);
			UIManager.Instance.ShowCaption(Constants.Texts.TimeIsUp, Constants.Texts.TapToRestart, false);
		}

		void OnDescriptionAutohide()
		{
			Play();
		}

		#endregion
	}
}
