namespace Konyisoft.Tilt
{
	public enum GameState
	{
		None,
		Title,
		LevelLoaded,
		Playing,
		Stopped,
		Waiting
	}

	public enum PlayResult
	{
		None,
		Success,
		Failed
	}
}