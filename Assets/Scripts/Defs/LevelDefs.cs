using System;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public enum Direction
	{
		None,
		Forward,
		Back,
		Left,
		Right
	}

	public enum NextMovement
	{
		Blocked,
		Moving,
		Teleporting,
		FallingOff,
		FallingOffInPlace,
		Die
	}

	[Flags]
	public enum MovementResult
	{
		None   = 0,
		Moved  = 1 << 1,
		Failed = 1 << 2
	}

	[Serializable]
	public class LevelItemData
	{
		public Color32 color;
		public LevelObject prefab;
	}

	[Serializable]
	public class LevelTileData
	{
		public Color32 color;
		public LevelObject prefab;
	}

	// TODO: use flags instead?
	[Serializable]
	public class AllowedDirections
	{
		public bool forward = true;
		public bool back = true;
		public bool left = true;
		public bool right = true;

		public bool IsAllowed(Direction direction)
		{
			return
				(direction == Direction.Forward && forward) ||
				(direction == Direction.Back && back) ||
				(direction == Direction.Left && left) ||
				(direction == Direction.Right && right);
		}

		public bool IsBlocked(Direction direction)
		{
			return !IsAllowed(direction);
		}
	}
}