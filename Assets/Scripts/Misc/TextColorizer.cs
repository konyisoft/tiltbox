using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	[RequireComponent(typeof(Text))]
	public class TextColorizer : MonoBehaviour
	{
		#region Fields

		public Gradient pulsatingColor = new Gradient();
		public float pulsatingSpeed = 1f;
		public bool pulsating = false;

		Text text;

		#endregion

		#region Mono methods

		void Awake()
		{
			text = GetComponent<Text>();
		}

		void Update()
		{
			if (text == null)
				return;

			// Pulsating (pingpong) of gradient color
			if (pulsating)
			{
				float pulsatingValue = Mathf.PingPong(Time.time * pulsatingSpeed, 1f);
				text.color = pulsatingColor.Evaluate(pulsatingValue);
			}
		}

		#endregion
	}
}
