using UnityEngine;

namespace Konyisoft.Tilt
{
	public class ParticleSystemAutoDestroy : MonoBehaviour
	{
		#region Fields
		
		ParticleSystem ps;

		#endregion

		#region Mono methods

		void Awake()
		{
			ps = GetComponent<ParticleSystem>();
		}

		void Update()
		{
			if (ps != null && !ps.IsAlive())
				Destroy(gameObject);
		}

		#endregion
	}
}