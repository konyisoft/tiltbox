using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	[AddComponentMenu("UI/Effects/TextGradient")]
	public class TextGradient : BaseMeshEffect
	{
		#region Structs

		[System.Serializable]
		public struct GradientColors
		{
			public Color32 startColor;
			public Color32 endColor;

			public GradientColors(Color32 startColor, Color32 endColor)
			{
				this.startColor = startColor;
				this.endColor = endColor;
			}
		}

		#endregion

		#region Enumerations

		public enum GradientType
		{
			Vertical,
			Horizontal
		}

		#endregion

		#region Fields

		[SerializeField]
		public GradientType gradientType = GradientType.Vertical;

		[SerializeField]
		[Range(-1.5f, 1.5f)]
		public float offset = 0f;

		[SerializeField]
		public GradientColors gradientColors;

		#endregion

		#region Public methods

		public override void ModifyMesh(VertexHelper helper)
		{
			if (!IsActive() || helper.currentVertCount == 0)
				return;

			List<UIVertex> vertexList = new List<UIVertex>();
			helper.GetUIVertexStream(vertexList);

			int vertexCount = vertexList.Count;
			switch (gradientType)
			{
				case GradientType.Vertical:
					{
						float fBottomY = vertexList[0].position.y;
						float fTopY = vertexList[0].position.y;
						float fYPos = 0f;

						for (int i = vertexCount - 1; i >= 1; --i)
						{
							fYPos = vertexList[i].position.y;
							if (fYPos > fTopY)
							{
								fTopY = fYPos;
							}
							else if (fYPos < fBottomY)
							{
								fBottomY = fYPos;
							}
						}

						float fUIElementHeight = 1f / (fTopY - fBottomY);
						UIVertex v = new UIVertex();

						for (int i = 0; i < helper.currentVertCount; i++)
						{
							helper.PopulateUIVertex(ref v, i);
							v.color = Color32.Lerp(gradientColors.endColor, gradientColors.startColor, (v.position.y - fBottomY) * fUIElementHeight - offset);
							helper.SetUIVertex(v, i);
						}
					}
					break;

				case GradientType.Horizontal:
					{
						float fLeftX = vertexList[0].position.x;
						float fRightX = vertexList[0].position.x;
						float fXPos = 0f;

						for (int i = vertexCount - 1; i >= 1; --i)
						{
							fXPos = vertexList[i].position.x;
							if (fXPos > fRightX)
							{
								fRightX = fXPos;
							}
							else if (fXPos < fLeftX)
							{
								fLeftX = fXPos;
							}
						}

						float fUIElementWidth = 1f / (fRightX - fLeftX);
						UIVertex v = new UIVertex();

						for (int i = 0; i < helper.currentVertCount; i++)
						{
							helper.PopulateUIVertex(ref v, i);
							v.color = Color32.Lerp(gradientColors.startColor, gradientColors.endColor, (v.position.x - fLeftX) * fUIElementWidth - offset);
							helper.SetUIVertex(v, i);
						}
					}
					break;
			}
		}

		#endregion
	}
}