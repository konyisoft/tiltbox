using UnityEngine;

namespace Konyisoft.Tilt
{
	[RequireComponent(typeof(MeshRenderer), typeof(TextMesh))]
	public class TextMeshColorizer : MonoBehaviour
	{
		#region Fields

		public Color staticColor = Color.white;
		public Gradient pulsatingColor = new Gradient();
		public float pulsatingSpeed = 1f;
		public bool pulsating = false;

		Material copyMaterial;

		#endregion

		#region Mono methods

		void Awake()
		{
			Renderer renderer = GetComponent<Renderer>();
			if (renderer != null && renderer.material != null)
			{
				copyMaterial = new Material(renderer.material);
				renderer.material = copyMaterial;
			}
		}

		void OnDestroy()
		{
			if (copyMaterial != null)
			{
				Destroy(copyMaterial);
			}
		}

		void Update()
		{
			if (copyMaterial == null)
				return;

			// Pulsating (pingpong) of gradient color
			if (pulsating)
			{
				float pulsatingValue = Mathf.PingPong(Time.time * pulsatingSpeed, 1f);
				copyMaterial.color = pulsatingColor.Evaluate(pulsatingValue);
			}
			// Change to static color
			else if (staticColor != copyMaterial.color)
			{
				copyMaterial.color = staticColor;
			}
		}

		#endregion
	}
}