using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public static class MathUtils
	{
		public static float Normalize(float value, float min, float max)
		{
			return (value - min) / (max - min);
		}
		
		public static float RoundToDecimals(float value, int decimals)
		{
			float mult = Mathf.Pow(10.0f, (float)decimals);
			return Mathf.Round(value * mult) / mult;
		}
	}
}
