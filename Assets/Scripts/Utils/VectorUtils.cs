using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public static class VectorUtils
	{
		public static Vector3 RoundToDecimals(Vector3 v, int decimals)
		{
			v.x = MathUtils.RoundToDecimals(v.x, decimals);
			v.y = MathUtils.RoundToDecimals(v.y, decimals);
			v.z = MathUtils.RoundToDecimals(v.z, decimals);
			return v;
		}

		public static bool Vector3Approximately(Vector3 v1, Vector3 v2)
		{
			return
				Mathf.Approximately(v1.x, v2.x) &&
				Mathf.Approximately(v1.y, v2.y) &&
				Mathf.Approximately(v1.z, v2.z);
		}		
	}
}
