using UnityEngine;
using UnityEngine.EventSystems;

namespace Konyisoft.Tilt
{
	public static class InputUtils
	{
		#region Properties

		public static bool IsMouseOverUI
		{
			get
			{
				return EventSystem.current.IsPointerOverGameObject();
			}
		}

		public static bool IsAnyTouchOverUI
		{
			get
			{
				bool r = false;
				for (int i = 0; i < Input.touchCount; i++)
				{
					if (IsTouchOverUI(Input.touches[i].fingerId))
					{
						r = true;
						break;
					}
				}
				return r;
			}
		}

		public static bool HasBeganTouch
		{
			get
			{
				bool r = false;
				for (int i = 0; i < Input.touchCount; i++)
				{
					if (Input.touches[i].phase == TouchPhase.Began)
					{
						r = true;
						break;
					}
				}
				return r;
			}
		}

		#endregion

		#region Public methods

		public static bool IsTouchOverUI(int fingerId)
		{
			return EventSystem.current.IsPointerOverGameObject(fingerId);
		}

		public static Touch? GetTouchByFingerId(int fingerId)
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (fingerId == Input.touches[i].fingerId)
				{
					touch = Input.touches[i];
					break;
				}
			}
			return touch;
		}

		public static Touch? GetLastBeganTouch()
		{
			Touch? touch = null;
			for (int i = 0; i < Input.touchCount; i++)
			{
				Touch t = Input.touches[i];
				if (t.phase == TouchPhase.Began)
				{
					touch = t;
					break;
				}
			}
			return touch;
		}

		#endregion
	}
}
