using System;
using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public static class CoroutineUtils
	{
		public static IEnumerator CO_LerpFloat(float fromFloat, float toFloat, float duration,
			Action before, Action<float> action, Action after)
		{
			if (before != null)
			{
				before();
			}

			if (action != null)
			{
				float startTime = Time.time;
				action(fromFloat);
				while (Time.time - startTime < duration)
				{
					action(Mathf.Lerp(fromFloat, toFloat, (Time.time - startTime) / duration));
					yield return null;
				}
				action(toFloat);
			}

			if (after != null)
			{
				after();
			}
			
			yield return null;
		}

		public static IEnumerator CO_LerpVector3(Vector3 fromVector3, Vector3 toVector3, float duration,
			Action before, Action<Vector3> action, Action after)
		{
			if (before != null)
			{
				before();
			}

			if (action != null)
			{
				float startTime = Time.time;
				action(fromVector3);
				while (Time.time - startTime < duration)
				{
					action(Vector3.Lerp(fromVector3, toVector3, (Time.time - startTime) / duration));
					yield return null;
				}
				action(toVector3);
			}

			if (after != null)
			{
				after();
			}
			
			yield return null;
		}
	}
}
