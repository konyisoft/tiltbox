namespace Konyisoft.Tilt
{
	public class Level015 : LevelConcrete
	{
		#region Fields

		Rolling itemGreen;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			itemGreen = GetRollingByName(Constants.ObjectNames.ItemGreen);
			itemGreen.CanDie = true;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetTilesOfType<Vanishing>().Count == 0 && AllSlotsAreBusy<Rolling>();
		}

		#endregion
	}
}