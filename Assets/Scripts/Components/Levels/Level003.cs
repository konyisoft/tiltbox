namespace Konyisoft.Tilt
{
	public class Level003 : LevelConcrete
	{
		#region Fields

		Slot slotRed;
		Slot slotBlue;
		Rolling itemRed;
		Rolling itemBlue;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			
			slotRed = GetSlotByName(Constants.ObjectNames.SlotRed);
			slotBlue = GetSlotByName(Constants.ObjectNames.SlotBlue);

			itemRed = GetRollingByName(Constants.ObjectNames.ItemRed);
			itemBlue = GetRollingByName(Constants.ObjectNames.ItemBlue);
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemAbove(slotRed) == itemRed && GetItemAbove(slotBlue) == itemBlue;
		}

		#endregion
	}
}