using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Level : GameBehaviour
	{
		#region Events

		public delegate void OnSetNextMovementEvent(Direction direction);
		public event OnSetNextMovementEvent onBeforeSetNextMovement;
		public event OnSetNextMovementEvent onAfterSetNextMovement;

		public delegate void OnMovementEvent(MovementResult movementResult);
		public event OnMovementEvent onBeforeMoveItems;
		public event OnMovementEvent onAfterMoveItems;
		public event OnMovementEvent onAfterLevelRefresh;

		#endregion

		#region Fields

		public Texture2D itemsTexture;
		public Texture2D tilesTexture;
		public int time = 60;
		public string description = string.Empty;
		public Sprite background;
		public Sprite screenshot;
		public float cameraSizeAddition;

		protected Item[,] itemsArray;
		protected Tile[,] tilesArray;

		protected List<Item> itemsList = new List<Item>(); // easier to iterate through
		protected List<Tile> tilesList = new List<Tile>();

		#endregion

		#region Properties

		public bool IsCompleted
		{
			get { return CheckCompleted(); }
		}

		public bool IsFailed
		{
			get { return CheckFailed(); }
		}

		public int ColCount
		{
			get { return itemsArray != null ? itemsArray.GetLength(0) : 0; }
		}

		public int RowCount
		{
			get { return itemsArray != null ? itemsArray.GetLength(1) : 0; }
		}

		public string Name
		{
			get { return gameObject.name; }
		}

		#endregion

		#region Constants

		static Color32 NullColor = new Color32(0, 0, 0, 1); // full black is null color (no object instantiated)

		#endregion

		#region Mono methods

		protected virtual void Awake()
		{
			if (itemsTexture == null)
			{
				Debug.LogError("No items texture attached.", this);
				return;
			}

			if (tilesTexture == null)
			{
				Debug.LogError("No tiles texture attached.", this);
				return;
			}

			if (itemsTexture.width != tilesTexture.width || itemsTexture.height != tilesTexture.height)
			{
				Debug.LogError("Textures are required to have the same width and height.", this);
				return;
			}

			Create();
		}

		#endregion

		#region Protectd methods

		protected virtual bool CheckCompleted()
		{
			return false; // by default
		}

		protected virtual bool CheckFailed()
		{
			return false; // by default
		}

		#endregion

		#region Public methods

		public void Create()
		{
			// Instantiate tiles and items
			if (tilesTexture != null && itemsTexture != null)
			{
				InstantiateObjects();
			}
		}

		public void Clear()
		{
			IterateItems<Item>((item) =>
			{
				item.ReturnToPool();
			});

			IterateTiles<Tile>((tile) =>
			{
				tile.ReturnToPool();
			});

			itemsArray = null;
			tilesArray = null;
			itemsList.Clear();
			tilesList.Clear();
		}

		public Tile GetTileBelow(Item item)
		{
			return tilesArray[item.Col, item.Row] as Tile;
		}

		public Item GetItemAbove(Tile tile)
		{
			return itemsArray[tile.Col, tile.Row] as Item;
		}

		public Tile GetTileAt(int col, int row)
		{
			// NULL border around the array!
			col++;
			row++;
			row = tilesArray.GetLength(1) - 1 - row; // Reversed array
			return tilesList.Find(tile => col == tile.Col && row == tile.Row);
		}

		public Item GetItemAt(int col, int row)
		{
			// NULL border around the array!
			col++;
			row++;
			row = itemsArray.GetLength(1) - 1 - row; // Reversed array
			return itemsList.Find(item => col == item.Col && row == item.Row);
		}

		public T GetItemOfType<T>() where T : Item
		{
			// Get the first one found in the list
			Item item = itemsList.Find(i => i is T);
			return item != null ? item as T : null;
		}

		public List<T> GetItemsOfType<T>() where T : Item
		{
			List<T> list = new List<T>();
			IterateItems<T>((item) =>
			{
				list.Add(item);
			});
			return list;
		}

		public T GetTileOfType<T>() where T : Tile
		{
			// Get the first one found in the list
			Tile tile = tilesList.Find(t => t is T);
			return tile != null ? tile as T : null;
		}

		public List<T> GetTilesOfType<T>() where T : Tile
		{
			List<T> list = new List<T>();
			IterateTiles<T>((tile) =>
			{
				list.Add(tile);
			});
			return list;
		}

		public void RelocateItem(Item item, int col, int row)
		{
			if (item != null && CheckIndexes(itemsArray, col, row))
			{
				itemsArray[item.Col, item.Row] = null;
				itemsArray[col, row] = item;
				item.SetCoords(col, row);
			}
		}

		public void RelocateTile(Tile tile, int col, int row)
		{
			if (tile != null && CheckIndexes(tilesArray, col, row))
			{
				tilesArray[tile.Col, tile.Row] = null;
				tilesArray[col, row] = tile;
				tile.SetCoords(col, row);
			}
		}

		public MovementResult MoveItems(Direction direction)
		{
			MovementResult movementResult = MovementResult.None;

			// Event
			if (onBeforeSetNextMovement != null)
			{
				onBeforeSetNextMovement(direction);
			}

			// 1st iteration: calc and set items' next movement
			IterateItems<Movable>((movable) =>
			{
				movementResult |= movable.PrepareNextMovement(direction);
			});

			// Event
			if (onAfterSetNextMovement != null)
			{
				onAfterSetNextMovement(direction);
			}

			// 2nd iteration: do movement!
			if (movementResult != MovementResult.None)
			{
				// Event
				if (onBeforeMoveItems != null)
				{
					onBeforeMoveItems(movementResult);
				}

				IterateItems<Movable>((movable) =>
				{
					movable.DoMovement(direction);
				});

				// Event
				if (onAfterMoveItems != null)
				{
					onAfterMoveItems(movementResult);
				}

				RebuildItemsArray();
				RebuildTilesArray();

				// Event
				if (onAfterLevelRefresh != null)
				{
					onAfterLevelRefresh(movementResult);
				}
			}

			return movementResult;
		}

		public void IterateItems<T>(Action<T> action) where T : LevelObject
		{
			for (int i = 0; i < itemsList.Count; i++)
			{
				if (itemsList[i] is T)  // includes null checking
				{
					action(itemsList[i] as T);
				}
			}
		}

		public void IterateTiles<T>(Action<T> action) where T : LevelObject
		{
			for (int i = 0; i < tilesList.Count; i++)
			{
				if (tilesList[i] is T)  // includes null checking
				{
					action(tilesList[i] as T);
				}
			}
		}

		public Item GetNeighbourItemOf(Item item, Direction direction, int offset = 1)
		{
			return GetNeighbourOf<Item>(itemsArray, item, direction, offset);
		}

		public Tile GetNeighbourTileOf(Tile tile, Direction direction, int offset = 1)
		{
			return GetNeighbourOf<Tile>(tilesArray, tile, direction, offset);
		}

		public void RemoveItemFromList(Item item)
		{
			itemsList.Remove(item);
		}

		public void RemoveTileFromList(Tile tile)
		{
			tilesList.Remove(tile);
		}

		#endregion

		#region Private methods

		void InstantiateObjects()
		{
			float objectSize = LevelManager.Instance.objectSize;
			int textureWidth = itemsTexture.width;
			int textureHeight = itemsTexture.height;

			// Initialize arrays
			// NOTE: +2 will create "border" with null elements around items and tiles
			itemsArray = new Item[textureWidth + 2, textureHeight + 2];
			tilesArray = new Tile[textureWidth + 2, textureHeight + 2];

			// Get pixels of texture
			Color32[] itemsPixels = itemsTexture.GetPixels32();
			Array.Reverse(itemsPixels); // Upside down
			Color32[] tilesPixels = tilesTexture.GetPixels32();
			Array.Reverse(tilesPixels); // Upside down

			int pixelCount = itemsPixels.Length;

			// Column and row counters
			int col = 1; // Not 0 because of null border
			int row = 1;

			// Coords of the object instances (center = 0,0)
			float startX = (objectSize * 0.5f) - (textureWidth * 0.5f * objectSize);
			float startZ = (objectSize * 0.5f) - (textureHeight * 0.5f * objectSize);
			float x = startX;
			float z = startZ;

			// Going backward in pixel arrays
			for (int i = pixelCount - 1; i >= 0; i--)
			{
				// Items: Not null color?
				if (!itemsPixels[i].Compare(NullColor))
				{
					// Find and instantiate the prefab assigned to current pixel color
					LevelItemData levelItemData = LevelManager.Instance.GetLevelItemData(itemsPixels[i]);
					if (levelItemData != null && levelItemData.prefab != null)
					{
						// Get from pool
						Item item = levelItemData.prefab.GetPooledInstance<Item>();
						item.transform.position = new Vector3(x, 0, z) + LevelManager.Instance.itemsOffset;
						item.transform.SetParent(LevelManager.Instance.levelParent, false);
						item.gameObject.name = item.gameObject.name.RemoveCloneSuffix();
						item.Initialize(this, col, row);
						itemsArray[col, row] = item;
						itemsList.Add(item);
					}
				}

				// Tiles
				if (!tilesPixels[i].Compare(NullColor))
				{
					LevelTileData levelTileData = LevelManager.Instance.GetLevelTileData(tilesPixels[i]);
					if (levelTileData != null && levelTileData.prefab != null)
					{
						Tile tile = levelTileData.prefab.GetPooledInstance<Tile>();
						tile.transform.position = new Vector3(x, 0, z) + LevelManager.Instance.tilesOffset;
						tile.transform.SetParent(LevelManager.Instance.levelParent, false);
						tile.gameObject.name = tile.gameObject.name.RemoveCloneSuffix();
						tile.Initialize(this, col, row);
						tilesArray[col, row] = tile;
						tilesList.Add(tile);
					}
				}

				// Update coords
				col++;
				x += objectSize;

				if (col - 1 == textureWidth)
				{
					col = 1; // Not 0 because of null border
					row++;
					x = startX;
					z += objectSize;
				}
			}

			// Sort lists by object priority
			itemsList = itemsList.OrderBy(item => item.Priority).ToList();
			tilesList = tilesList.OrderBy(tile => tile.Priority).ToList();
		}

		void RebuildItemsArray()
		{
			itemsArray = new Item[itemsArray.GetLength(0), itemsArray.GetLength(1)]; // null array

			// Raw list is ordered by priority. An item with lower
			// priority won't override a higher one in the array.
			for (int i = 0; i < itemsList.Count; i++)
			{
				Item item = itemsList[i];
				if (itemsArray[item.Col, item.Row] == null && !item.HasDestroyed)
				{
					itemsArray[item.Col, item.Row] = item;
				}
			}
		}

		void RebuildTilesArray()
		{
			tilesArray = new Tile[tilesArray.GetLength(0), tilesArray.GetLength(1)]; // null array
			for (int i = 0; i < tilesList.Count; i++)
			{
				Tile tile = tilesList[i];
				if (tilesArray[tile.Col, tile.Row] == null && !tile.HasDestroyed)
				{
					tilesArray[tile.Col, tile.Row] = tile;
				}
			}
		}

		T GetNeighbourOf<T>(LevelObject[,] array, T levelObject, Direction direction, int offset = 1) where T : LevelObject
		{
			// Current coords
			int col = levelObject.Col;
			int row = levelObject.Row;

			switch (direction)
			{
				case Direction.Forward:
					row += offset;
					break;
				case Direction.Back:
					row -= offset;
					break;
				case Direction.Left:
					col -= offset;
					break;
				case Direction.Right:
					col += offset;
					break;
			}

			// Array indexes check
			if (col < 0 || col > array.GetLength(0) - 1 || row < 0 || row > array.GetLength(1) - 1)
			{
				return null; // out of array
			}
			else
			{
				return array[col, row] as T;  // could be null: no neighbour object
			}
		}

		bool CheckIndexes(LevelObject[,] array, int col, int row)
		{
			return
				col >= 0 && col < array.GetLength(0) &&
				row >= 0 && row < array.GetLength(1);
		}


		#endregion
	}
}
