using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class Level005 : LevelConcrete
	{
		#region Fields

		List<Rolling> itemsGreen;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			itemsGreen = GetRollingsByName(Constants.ObjectNames.ItemGreen);
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			// Check all green ones
			for (int i = 0; i < itemsGreen.Count; i++)
			{
				// Returns true, if the two right or down neighbours are green cubes
				Rolling rolling = itemsGreen[i];
				if (CheckTwoNeighboursOf(rolling, Direction.Right) || CheckTwoNeighboursOf(rolling, Direction.Back))
				{
					return true;
				}
			}
			return false;
		}

		#endregion

		#region Private methods

		bool CheckTwoNeighboursOf(Item item, Direction direction)
		{
			Item item1 = GetNeighbourItemOf(item, direction, 1);
			Item item2 = GetNeighbourItemOf(item, direction, 2);

			return 
				item1 is Rolling && item1.Name == Constants.ObjectNames.ItemGreen &&
				item2 is Rolling && item2.Name == Constants.ObjectNames.ItemGreen;
		}

		#endregion
	}
}