namespace Konyisoft.Tilt
{
	public class Level004 : LevelConcrete
	{
		#region Fields

		Slot slotYellow;
		Rolling itemYellow;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slotYellow = GetSlotByName(Constants.ObjectNames.SlotYellow);
			itemYellow = GetRollingByName(Constants.ObjectNames.ItemYellow);
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemAbove(slotYellow) == itemYellow;
		}

		#endregion
	}
}