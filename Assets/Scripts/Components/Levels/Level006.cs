namespace Konyisoft.Tilt
{
	public class Level006 : LevelConcrete
	{
		#region Protected methods

		protected override bool CheckCompleted()
		{
			return AllSlotsAreBusy<Rolling>();
		}

		#endregion
	}
}