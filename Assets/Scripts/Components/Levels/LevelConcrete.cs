using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class LevelConcrete : Level
	{
		#region Fields
		
		protected List<Slot> slots;
		protected List<Rolling> rollings;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slots = GetTilesOfType<Slot>();
			rollings = GetItemsOfType<Rolling>();
		}

		#endregion

		#region Protected methods

		protected Slot GetSlotByName(string name)
		{
			return slots.Find(slot => slot.Name == name);
		}

		protected List<Slot> GetSlotsByName(string name)
		{
			return slots.FindAll(slot => slot.Name == name);
		}

		protected Rolling GetRollingByName(string name)
		{
			return rollings.Find(rolling => rolling.Name == name);
		}

		protected List<Rolling> GetRollingsByName(string name)
		{
			return rollings.FindAll(rolling => rolling.Name == name);
		}

		protected bool AllSlotsAreBusy<T>() where T : Movable
		{
			// No slots
			if (slots.Count < 1)
			{
				return false;
			}
			
			for (int i = 0; i < slots.Count; i++)
			{
				// If no (rolling) item above a slot tile, the condition fails
				if ((GetItemAbove(slots[i]) is T) == false)
				{
					return false;
				}
			}

			return true;
		}

		#endregion
	}
}
