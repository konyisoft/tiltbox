namespace Konyisoft.Tilt
{
	public class Level008 : LevelConcrete
	{
		#region Fields

		Slot slotRed;
		Slot slotOrange;
		Slot slotYellow;
		Slot slotGreen;
		Slot slotBlue;
		Slot slotMagenta;

		Rolling itemRed;
		Rolling itemOrange;
		Rolling itemYellow;
		Rolling itemGreen;
		Rolling itemBlue;
		Rolling itemMagenta;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			slotRed = GetSlotByName(Constants.ObjectNames.SlotRed);
			slotOrange = GetSlotByName(Constants.ObjectNames.SlotOrange);
			slotYellow = GetSlotByName(Constants.ObjectNames.SlotYellow);
			slotGreen = GetSlotByName(Constants.ObjectNames.SlotGreen);
			slotBlue = GetSlotByName(Constants.ObjectNames.SlotBlue);
			slotMagenta = GetSlotByName(Constants.ObjectNames.SlotMagenta);

			itemRed = GetRollingByName(Constants.ObjectNames.ItemRed);
			itemOrange = GetRollingByName(Constants.ObjectNames.ItemOrange);
			itemYellow = GetRollingByName(Constants.ObjectNames.ItemYellow);
			itemGreen = GetRollingByName(Constants.ObjectNames.ItemGreen);
			itemBlue = GetRollingByName(Constants.ObjectNames.ItemBlue);
			itemMagenta = GetRollingByName(Constants.ObjectNames.ItemMagenta);
		}

		#endregion
		
		#region Protected methods

		protected override bool CheckCompleted()
		{
			return 
				GetItemAbove(slotRed) == itemRed &&
				GetItemAbove(slotOrange) == itemOrange &&
				GetItemAbove(slotYellow) == itemYellow &&
				GetItemAbove(slotGreen) == itemGreen &&
				GetItemAbove(slotBlue) == itemBlue &&
				GetItemAbove(slotMagenta) == itemMagenta;
		}

		#endregion
	}
}
