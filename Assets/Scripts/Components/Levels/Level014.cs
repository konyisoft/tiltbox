using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Level014 : LevelConcrete
	{
		#region Fields

		public Color32[] colors = new Color32[0];
		public int startColorIndex;
		public int winningColorIndex;

		List<ColorSwapper> colorSwappers;
		Slot slotWhite;
		Rolling itemWhite;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			slotWhite = GetSlotByName(Constants.ObjectNames.SlotWhite);
			itemWhite = GetRollingByName(Constants.ObjectNames.ItemWhite);
			itemWhite.CanDie = true;

			colorSwappers = GetTilesOfType<ColorSwapper>();

			// Setting up swapper tiles
			for (int i = 0; i < colorSwappers.Count; i++)
			{
				ColorSwapper colorSwapper = colorSwappers[i];
				colorSwapper.SetColors(colors);
				colorSwapper.SetStartColorIndex(startColorIndex);
			}
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			bool result = true; // by default
			
			// Check colors
			for (int i = 0; i < colorSwappers.Count; i++)
			{
				if (colorSwappers[i].ColorIndex != winningColorIndex)
				{
					return false; // returns immediately
				}
			}

			// Check target slot
			return result && GetItemAbove(slotWhite) == itemWhite;
		}

		#endregion
	}
}