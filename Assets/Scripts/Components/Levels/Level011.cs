using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class Level011 : LevelConcrete
	{
		#region Fields

		Slot slotWhite;
		List<Slot> slotsBlue;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slotWhite = GetSlotByName(Constants.ObjectNames.SlotWhite);
			slotsBlue = GetSlotsByName(Constants.ObjectNames.SlotBlue);
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			// Check blue slots
			for (int i = 0; i < slotsBlue.Count; i++)
			{
				Item itemAboveSlotBlue = GetItemAbove(slotsBlue[i]);
				if (!(itemAboveSlotBlue is Rolling) || itemAboveSlotBlue.Name != Constants.ObjectNames.ItemBlue)
				{
					return false;
				}
			}

			// Check white slot
			Item itemAboveSlotWhite = GetItemAbove(slotWhite);
			return itemAboveSlotWhite is Rolling && itemAboveSlotWhite.Name == Constants.ObjectNames.ItemWhite;
		}

		#endregion
	}
}