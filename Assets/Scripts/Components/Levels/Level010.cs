namespace Konyisoft.Tilt
{
	public class Level010 : LevelConcrete
	{
		#region Fields

		Rolling itemYellow;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			itemYellow = GetRollingByName(Constants.ObjectNames.ItemYellow);
			itemYellow.CanCollectItems = true;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemsOfType<Collectible>().Count == 0;
		}

		protected override bool CheckFailed()
		{
			// Failed if any of its neighbour is a rolling item
			return
				GetNeighbourItemOf(itemYellow, Direction.Forward) is Rolling ||
				GetNeighbourItemOf(itemYellow, Direction.Back) is Rolling ||
				GetNeighbourItemOf(itemYellow, Direction.Left) is Rolling ||
				GetNeighbourItemOf(itemYellow, Direction.Right) is Rolling;
		}

		#endregion
	}
}