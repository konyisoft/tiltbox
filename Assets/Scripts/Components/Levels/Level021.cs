using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Level021 : LevelConcrete
	{
		#region Fields

		Slot slotOrange;
		Slot slotYellow;
		Slot slotRed;
		Slot slotMagenta;
		Rolling itemOrange;
		Rolling itemYellow;
		Rolling itemRed;
		Rolling itemMagenta;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slotOrange = GetSlotByName(Constants.ObjectNames.SlotOrange);
			slotYellow = GetSlotByName(Constants.ObjectNames.SlotYellow);
			slotRed = GetSlotByName(Constants.ObjectNames.SlotRed);
			slotMagenta = GetSlotByName(Constants.ObjectNames.SlotMagenta);
			
			itemOrange = GetRollingByName(Constants.ObjectNames.ItemOrange);
			itemYellow = GetRollingByName(Constants.ObjectNames.ItemYellow);
			itemRed = GetRollingByName(Constants.ObjectNames.ItemRed);
			itemMagenta = GetRollingByName(Constants.ObjectNames.ItemMagenta);
			
			// All items can die
			itemOrange.CanDie = true;
			itemYellow.CanDie = true;
			itemRed.CanDie = true;
			itemMagenta.CanDie = true;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return
				GetItemAbove(slotOrange) == itemOrange &&
				GetItemAbove(slotYellow) == itemYellow &&
				GetItemAbove(slotRed) == itemRed &&
				GetItemAbove(slotMagenta) == itemMagenta;
		}

		#endregion
	}
}