namespace Konyisoft.Tilt
{
	public class Level019 : LevelConcrete
	{
		#region Fields

		TrapDoor trapDoor;
		Switcher switcher;
		Slot slotYellow;
		Slot slotOrange;
		Rolling itemYellow;
		Rolling itemOrange;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			// 1 trapdoor and 1 switcher tile must exist in this level
			trapDoor = GetTileOfType<TrapDoor>();
			trapDoor.Open(true);
			switcher = GetTileOfType<Switcher>();
			switcher.onStateChange += OnStateChange;

			slotYellow = GetSlotByName(Constants.ObjectNames.SlotYellow);
			slotOrange = GetSlotByName(Constants.ObjectNames.SlotOrange);
			itemYellow = GetRollingByName(Constants.ObjectNames.ItemYellow);
			itemOrange = GetRollingByName(Constants.ObjectNames.ItemOrange);
		}

		void OnDestroy()
		{
			switcher.onStateChange -= OnStateChange;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return
				GetItemAbove(slotYellow) == itemYellow &&
				GetItemAbove(slotOrange) == itemOrange;
		}

		#endregion

		#region Event handlers

		void OnStateChange(Switcher.SwitchState state)
		{
			switch (state)
			{
				case Switcher.SwitchState.Off:
					trapDoor.Open();
					break;
				case Switcher.SwitchState.On:
					trapDoor.Close();
					break;
			}
		}

		#endregion
	}
}