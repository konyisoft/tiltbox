namespace Konyisoft.Tilt
{
	public class Level016 : LevelConcrete
	{
		#region Fields

		Slot slotOrange;
		Rolling itemOrange;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slotOrange = GetSlotByName(Constants.ObjectNames.SlotOrange);
			itemOrange = GetRollingByName(Constants.ObjectNames.ItemOrange);
			itemOrange.CanDie = true;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemAbove(slotOrange) == itemOrange;
		}

		#endregion
	}
}