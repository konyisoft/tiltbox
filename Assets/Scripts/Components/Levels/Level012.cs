using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class Level012 : LevelConcrete
	{
		#region Fields

		Slot slotRed;
		Slot slotOrange;
		
		Rolling itemRed;
		Rolling itemOrange;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			slotRed = GetSlotByName(Constants.ObjectNames.SlotRed);
			slotOrange = GetSlotByName(Constants.ObjectNames.SlotOrange);

			itemRed = GetRollingByName(Constants.ObjectNames.ItemRed);
			itemRed.CanDie = true;

			itemOrange = GetRollingByName(Constants.ObjectNames.ItemOrange);
			itemOrange.CanDie = true;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemAbove(slotRed) == itemRed && GetItemAbove(slotOrange) == itemOrange;
		}

		#endregion
	}
}