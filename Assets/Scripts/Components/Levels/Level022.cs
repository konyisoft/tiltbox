using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Level022 : LevelConcrete
	{
		#region Fields

		#endregion

		#region Mono methods

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return AllSlotsAreBusy<SlidingPushable>();
		}

		#endregion
	}
}