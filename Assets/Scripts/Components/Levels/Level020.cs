namespace Konyisoft.Tilt
{
	public class Level020 : LevelConcrete
	{
		#region Fields

		Rolling itemMagenta;
		Rolling itemBlue;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			itemMagenta = GetRollingByName(Constants.ObjectNames.ItemMagenta);
			itemBlue = GetRollingByName(Constants.ObjectNames.ItemBlue);
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return
				GetTileBelow(itemMagenta) is Slot &&
				GetTileBelow(itemBlue) is Slot &&
				GetTilesOfType<Vanishing>().Count == 0;
		}

		#endregion
	}
}