namespace Konyisoft.Tilt
{
	public class Level009 : LevelConcrete
	{
		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetTilesOfType<Vanishing>().Count == 0 && AllSlotsAreBusy<Rolling>();
		}

		#endregion
	}
}