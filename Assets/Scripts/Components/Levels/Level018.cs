using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Level018 : LevelConcrete
	{
		#region Fields

		List<Dice> dices;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			dices = GetItemsOfType<Dice>();
			RandomizeRotations();
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			bool result = true; // by default
			for (int i = 0; i < dices.Count; i++)
			{
				// Doesn't show 6? Return false.
				if (dices[i].Number != 6)
				{
					result = false;
					break;
				}
			}
			return result;
		}

		#endregion

		#region Private methods

		void RandomizeRotations()
		{
			dices.ForEach(dice =>
			{
				// Random rotation. 6 is not allowed.
				switch (Random.Range(0, 6) + 1)
				{
					case 1:
						// Do nothing, 1 remains on top
						break;
					case 2:
						dice.transform.Rotate(-90f, 0, 0, Space.Self);
						break;
					case 3:
						dice.transform.Rotate(0, 0, 90f, Space.Self);
						break;
					case 4:
						dice.transform.Rotate(0, 0, -90f, Space.Self);
						break;
					case 5:
						dice.transform.Rotate(90f, 0, 0, Space.Self);
						break;
				}

				// Y
				switch (Random.Range(0, 4) + 1)
				{
					case 1:
						dice.transform.Rotate(0, 90f, 0, Space.Self);
						break;
					case 2:
						dice.transform.Rotate(0, 180f, 0, Space.Self);
						break;
					case 3:
						dice.transform.Rotate(0, 270f, 0, Space.Self);
						break;
				}
				dice.RecalculateNumber();
			});
		}

		#endregion
	}
}