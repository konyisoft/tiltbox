using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	// 7,2   2,7
	public class Level023 : LevelConcrete
	{
		#region Fields

		Teleport teleport1;
		Teleport teleport2;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			Tile teleportTile1 = GetTileAt(7, 2);
			Tile teleportTile2 = GetTileAt(2, 7);

			if (teleportTile1 is Teleport && teleportTile2 is Teleport)
			{
				teleport1 = teleportTile1 as Teleport;
				teleport2 = teleportTile2 as Teleport;
				teleport1.Connect(teleport2);
				teleport2.Connect(teleport1);
			}
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return false;
		}

		#endregion
	}
}