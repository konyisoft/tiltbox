using System;
using System.Collections.Generic;

namespace Konyisoft.Tilt
{
	public class Level017 : LevelConcrete
	{
		#region Fields

		Slot slotBlue;
		Slot slotMagenta;
		Rolling itemBlue;
		Rolling itemMagenta;
		List<TrapDoor> trapDoors;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			slotBlue = GetSlotByName(Constants.ObjectNames.SlotBlue);
			slotMagenta = GetSlotByName(Constants.ObjectNames.SlotMagenta);
			itemBlue = GetRollingByName(Constants.ObjectNames.ItemBlue);
			itemMagenta = GetRollingByName(Constants.ObjectNames.ItemMagenta);
			trapDoors = GetTilesOfType<TrapDoor>();
			onBeforeSetNextMovement += OnBeforeSetNextMovement;
		}

		void OnDestroy()
		{
			onBeforeSetNextMovement -= OnBeforeSetNextMovement;
		}

		#endregion

		#region Protected methods

		protected override bool CheckCompleted()
		{
			return GetItemAbove(slotBlue) == itemBlue && GetItemAbove(slotMagenta) == itemMagenta;
		}

		#endregion

		#region Event handlers

		void OnBeforeSetNextMovement(Direction direction)
		{
			bool blocked = true; // by default
			for (int i = 0; i < rollings.Count; i++)
			{
				// Precalculate next movement
				if (rollings[i].CalcNextMovement(direction) != NextMovement.Blocked)
				{
					blocked = false;
					break;
				}
			}

			// Won't toggle if all movable blocked
			if (!blocked)
			{
				trapDoors.ForEach(trapDoor => trapDoor.Toggle());
			}
		}

		#endregion
	}
}