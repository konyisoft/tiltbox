using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIButtonScaler : GameBehaviour
	{
		#region Fields

		public Image forward;
		public Image back;
		public Image left;
		public Image right;

		#endregion

		#region Properties

		bool AllButtonAttached
		{
			get { return forward != null && back != null && left != null && right != null; }
		}

		#endregion

		#region Public fields

		public void SetScale(float value)
		{
			if (!AllButtonAttached)
				return;

			Vector3 scale = new UnityEngine.Vector3(value, value, value);
			forward.transform.localScale = scale;
			back.transform.localScale = scale;
			left.transform.localScale = scale;
			right.transform.localScale = scale;
		}

		public void SetGap(float value)
		{
			if (!AllButtonAttached)
				return;

			(forward.transform as RectTransform).anchoredPosition = new Vector2(value / 2f, value);
			(back.transform as RectTransform).anchoredPosition = new Vector2(value / 2f, 0);
			(left.transform as RectTransform).anchoredPosition = new Vector2(0, value / 2f);
			(right.transform as RectTransform).anchoredPosition = new Vector2(value, value / 2f);
		}

		#endregion
	}
}