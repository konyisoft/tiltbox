using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UITitlePanel : GameBehaviour
	{
		#region Classes

		class LevelSlotData
		{
			public int index;
			public Level level;
			public GameObject slotObject;

			public void Update()
			{
				if (level != null && slotObject != null)
				{
					LevelPrefs.LevelPrefData prefData = LevelPrefs.GetData(level.Name);
					slotObject.transform.Find("Screenshot").GetComponent<Image>().sprite = level.screenshot;
					slotObject.transform.Find("Name").GetComponent<Text>().text = string.Format(Constants.Texts.Level, index + 1);

					Text movesText = slotObject.transform.Find("Moves").GetComponent<Text>();
					Text timeText = slotObject.transform.Find("Time").GetComponent<Text>();
					Text unsolvedText = slotObject.transform.Find("Unsolved").GetComponent<Text>();

					if (prefData == null)
					{
						movesText.text = string.Format(Constants.Texts.Moves, 0);
						timeText.text = string.Format(Constants.Texts.Time, Timer.GetTimeString(0));
						movesText.gameObject.SetActive(false);
						timeText.gameObject.SetActive(false);
						unsolvedText.gameObject.SetActive(true);
					}
					else
					{
						movesText.text = string.Format(Constants.Texts.Moves, prefData.moves);
						timeText.text = string.Format(Constants.Texts.Time, Timer.GetTimeString(prefData.time));
						movesText.gameObject.SetActive(true);
						timeText.gameObject.SetActive(true);
						unsolvedText.gameObject.SetActive(false);
					}
				}
			}
		}

		#endregion

		#region Fields

		public GameObject levelSlotPrefab;
		public Sprite background;
		public Text versionText;
		public GameObject logoObject;

		List<LevelSlotData> slots;

		#endregion

		#region Properties

		public bool Visible
		{
			get { return gameObject.activeInHierarchy; }
		}

		#endregion

		#region Mono methods

		void Start()
		{
			CreateLevelItems();
			
			if (versionText != null)
			{
				versionText.text = Application.version;
			}
		}

		#endregion

		#region Public methods

		public void Show()
		{
			gameObject.SetActive(true);
			SetLogoActive(true);
			UpdateSlots();
		}

		public void Hide()
		{
			gameObject.SetActive(false);
			SetLogoActive(false);
		}

		public void UpdateSlots()
		{
			if (slots == null)
				return;

			for (int i = 0; i < slots.Count; i++)
			{
				slots[i].Update();
			}
		}

		#endregion

		#region Private methods

		void CreateLevelItems()
		{
			slots = new List<LevelSlotData>();

			if (levelSlotPrefab != null)
			{
				for (int i = 0; i < LevelManager.Instance.levels.Length; i++)
				{
					int index = i;
					Level level = LevelManager.Instance.levels[i];
					GameObject slotObject = Instantiate(levelSlotPrefab);
					slots.Add(new LevelSlotData()
					{
						index = index,
						level = level,
						slotObject = slotObject
					});
					slotObject.transform.SetParent(levelSlotPrefab.transform.parent, false);
					slotObject.transform.GetComponent<Button>().onClick.AddListener(delegate { OnSlotClick(index); });
					slotObject.SetActive(true);
				}
				levelSlotPrefab.SetActive(false);
				//UpdateSlots(); // not needed here
			}
		}

		void SetLogoActive(bool active)
		{
			if (logoObject != null)
			{
				logoObject.SetActive(active);
			}
		}

		#endregion

		#region Event handlers

		void OnSlotClick(int levelIndex)
		{
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
			GameManager.Instance.NewGame(levelIndex);
		}

		public void OnButtonOptionsClick()
		{
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
			UIManager.Instance.ShowOptions();
		}

		#endregion
	}
}
