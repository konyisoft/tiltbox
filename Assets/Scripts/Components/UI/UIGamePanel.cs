using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIGamePanel : GameBehaviour
	{
		#region Fields

		public Text moveCounterText;
		public Text timerText;
		public float showHideDuration = 0.25f;

		CanvasGroup canvasGroup;

		#endregion

		#region Properties

		public bool Visible
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();
			Hide(true); // invisible by default
		}

		void OnDestroy()
		{
			StopAllCoroutines();
		}

		#endregion

		#region Public methods

		public void Show(bool immediate = false)
		{
			StopAllCoroutines();
			float targetAlpha = 1f;

			if (immediate)
			{
				SetAlpha(targetAlpha);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					GetAlpha(),
					targetAlpha,
					showHideDuration,
					null,
					(value) =>
					{
						SetAlpha(value);
					},
					null
				));
			}

			Visible = true;			
		}

		public void Hide(bool immediate = false)
		{
			StopAllCoroutines();
			float targetAlpha = 0;

			if (immediate)
			{
				SetAlpha(targetAlpha);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					GetAlpha(),
					targetAlpha,
					showHideDuration,
					null,
					(value) =>
					{
						SetAlpha(value);
					},
					null
				));
			}

			Visible = false;			
		}

		public void SetMoveCounterText(string text)
		{
			if (moveCounterText != null)
			{
				moveCounterText.text = text;
			}
		}

		public void SetTimerText(string text)
		{
			if (timerText != null)
			{
				timerText.text = text;
			}
		}

		#endregion

		#region Private methods

		void SetAlpha(float alpha)
		{
			if (canvasGroup != null)
			{
				canvasGroup.alpha = alpha;
			}
		}

		float GetAlpha()
		{
			return canvasGroup != null ? canvasGroup.alpha : 0;
		}

		#endregion

		#region Event handlers

		public void OnButtonRestartClick()
		{
			if (!Visible)
				return;
			
			// Display confirm dialog
			if (GameManager.Instance.GameState == GameState.Playing)
			{
				UIManager.Instance.ShowDialog(
					Constants.Texts.ConfirmRestart,
					new string[] { Constants.Texts.Yes, Constants.Texts.No },
					OnRestartDialog
				);
				AudioManager.Instance.PlaySound(Constants.Audio.Click);
			}
			// Restart without questioning
			else if (GameManager.Instance.GameState == GameState.Stopped)
			{
				GameManager.Instance.RestartLevel();
				AudioManager.Instance.PlaySound(Constants.Audio.Click);
			}
		}

		void OnRestartDialog(int buttonIndex)
		{
			if (buttonIndex == 0) // Yes
			{
				// Simple reload
				GameManager.Instance.RestartLevel();
			}
		}

		public void OnDescriptionClick()
		{
			if (!Visible)
				return;

			// Only during play
			if (GameManager.Instance.GameState == GameState.Playing && LevelManager.Instance.HasLevelLoaded)
			{
				string text = 
					//string.Format(Constants.Texts.Level, LevelManager.Instance.LevelIndex + 1) + "\n" +
					LevelManager.Instance.Level.description;

				UIManager.Instance.ShowDialog(
					text,
					new string[] { Constants.Texts.OK },
					null
				);
				AudioManager.Instance.PlaySound(Constants.Audio.Click);
			}
		}

		public void OnButtonExitClick()
		{
			if (!Visible)
				return;

			if (GameManager.Instance.GameState != GameState.Title)
			{
				UIManager.Instance.ShowDialog(
					Constants.Texts.ConfirmExit,
					new string[] { Constants.Texts.Yes, Constants.Texts.No },
					OnExitDialog
				);
				AudioManager.Instance.PlaySound(Constants.Audio.Click);
			}
		}

		void OnExitDialog(int buttonIndex)
		{
			if (buttonIndex == 0) // Yes
			{
				// Go to the title
				GameManager.Instance.TitleScreen();
			}
		}

		#endregion
	}
}