using System;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIOptionsPanel : GameBehaviour
	{
		#region Fields

		public Slider scaleSlider;
		public Slider gapSlider;
		public UIButtonScaler buttonScaler;
		public Text audioText;
		public Text timeLimitText;

		#endregion

		#region Properties

		public bool Visible
		{
			get { return gameObject.activeInHierarchy; }
		}

		#endregion

		#region Public methods

		public void Show()
		{
			Setup();
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		#endregion

		#region Private methods

		void Setup()
		{
			if (buttonScaler == null)
				return;

			if (scaleSlider != null)
			{
				scaleSlider.value = Options.buttonScale;
				buttonScaler.SetScale(Options.buttonScale);
			}

			if (gapSlider != null)
			{
				gapSlider.value = Options.buttonGap;
				buttonScaler.SetGap(Options.buttonGap);
			}

			SetAudioText(Options.audio);
			SetTimeLimitText(Options.timeLimit);
		}

		void SetAudioText(Options.AudioSetting audio)
		{
			if (audioText != null)
			{
				audioText.text = Constants.Texts.AudioSettings[(int)audio];
			}
		}

		void SetTimeLimitText(bool timeLimit)
		{
			if (timeLimitText != null)
			{
				timeLimitText.text = timeLimit ?
					Constants.Texts.Enabled :
					Constants.Texts.Disabled;
			}
		}

		#endregion

		#region Event handlers

		public void OnButtonDefaultsClick()
		{
			// Restore original settings
			Options.RestoreDefaults();
			Setup();
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
		}

		public void OnButtonBackClick()
		{
			// Save settings and hide
			if (scaleSlider != null)
			{
				Options.buttonScale = scaleSlider.value;
			}

			if (gapSlider != null)
			{
				Options.buttonGap = gapSlider.value;
			}

			Options.Save();
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
			Hide();
		}

		public void OnAudioTextClick()
		{
			int currentAudio = (int)Options.audio;
			if (currentAudio == 3)
			{
				currentAudio = 0;
			}
			else
			{
				currentAudio++;
			}
			Options.audio = (Options.AudioSetting)currentAudio;
			SetAudioText(Options.audio);
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
		}

		public void OnTimeLimitTextClick()
		{
			Options.timeLimit = !Options.timeLimit;
			SetTimeLimitText(Options.timeLimit);
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
		}

		public void OnResetAllTextClick()
		{
			UIManager.Instance.ShowDialog(
				Constants.Texts.ConfirmResetAll,
				new string[] { Constants.Texts.Yes, Constants.Texts.No },
				OnResetAllDialog
			);
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
		}

		public void OnScaleValueChanged()
		{
			if (buttonScaler != null && scaleSlider != null)
			{
				buttonScaler.SetScale(scaleSlider.value);
			}
		}

		public void OnGapValueChanged()
		{
			if (buttonScaler != null && gapSlider != null)
			{
				buttonScaler.SetGap(gapSlider.value);
			}
		}

		void OnResetAllDialog(int buttonIndex)
		{
			if (buttonIndex == 0)
			{
				LevelPrefs.Clear();
				UIManager.Instance.titlePanel.UpdateSlots();
			}
		}

		#endregion
	}
}
