using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	[RequireComponent(typeof(Image))]
	public class UIBackground : GameBehaviour
	{
		#region Fields

		public float moveLimit = 30f;
		public float speed = 5f;

		RectTransform rectTransform;
		Vector2 desiredOffsetMin;
		Vector2 desiredOffsetMax;

		#endregion

		#region Mono methods

		void Awake()
		{
			rectTransform = transform as RectTransform;
		}

		void Update()
		{
			rectTransform.offsetMin = Vector2.Lerp(rectTransform.offsetMin, desiredOffsetMin, Time.deltaTime * speed);
			rectTransform.offsetMax = Vector2.Lerp(rectTransform.offsetMax, desiredOffsetMax, Time.deltaTime * speed);
		}

		#endregion

		#region Public methods

		public void ResetMove()
		{
			desiredOffsetMin = Vector2.zero;
			desiredOffsetMax = Vector2.zero;
		}
		
		public void Move(Direction direction)
		{
			desiredOffsetMin = Vector2.zero;
			desiredOffsetMax = Vector2.zero;

			switch (direction)
			{
				case Direction.Forward:
					desiredOffsetMin.y = -moveLimit;
					break;
				case Direction.Back:
					desiredOffsetMax.y = moveLimit;
					break;
				case Direction.Left:
					desiredOffsetMax.x = moveLimit;
					break;
				case Direction.Right:
					desiredOffsetMin.x = -moveLimit;
					break;
			}
		}

		public void SetSprite(Sprite sprite)
		{
			Image image = GetComponent<Image>();
			if (image != null)
			{
				image.sprite = sprite;
			}
		}

		#endregion
	}
}