using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIButtonPanel : GameBehaviour
	{
		#region Fields

		UIButtonScaler buttonScaler;

		#endregion

		#region Properties

		public bool Visible
		{
			get { return gameObject.activeInHierarchy; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			buttonScaler = GetComponent<UIButtonScaler>();
		}

		#endregion

		#region Public methods

		public void Show()
		{
			Setup();
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		#endregion

		#region Private methods

		void Setup()
		{
			if (buttonScaler == null)
				return;

			buttonScaler.SetScale(Options.buttonScale);
			buttonScaler.SetGap(Options.buttonGap);
		}

		#endregion
	}
}
