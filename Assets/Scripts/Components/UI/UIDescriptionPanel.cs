using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIDescriptionPanel : GameBehaviour
	{
		#region Events

		public delegate void OnAutohide();
		public event OnAutohide onAutoHide;

		#endregion

		#region Fields

		public Text descriptionText;
		public float showHideDuration = 0.25f;
		public bool autohide = true;
		public float autohideDelay = 5f;

		RectTransform rectTransform;

		#endregion

		#region Properties

		public bool Visible
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			rectTransform = transform as RectTransform;
			Hide(true); // invisible by default
		}

		void OnDestroy()
		{
			StopAllCoroutines();
		}

		#endregion

		#region Public methods

		public void Show(string text, bool immediate = false)
		{
			StopAllCoroutines();
			SetText(text);
			float targetPosition = -rectTransform.rect.height;

			if (immediate)
			{
				SetVerticalPosition(targetPosition);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					rectTransform.anchoredPosition.y,
					targetPosition,
					showHideDuration,
					null,
					(value) =>
					{
						SetVerticalPosition(value);
					},
					() =>
					{
						if (autohide)
						{
							StartCoroutine(CO_Autohide());
						}
					}
				));
			}

			Visible = true;			
		}

		public void Hide(bool immediate = false)
		{
			StopAllCoroutines();
			float targetPosition = 0;

			if (immediate)
			{
				SetVerticalPosition(targetPosition);
				SetText(string.Empty);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					rectTransform.anchoredPosition.y,
					targetPosition,
					showHideDuration,
					null,
					(value) =>
					{
						SetVerticalPosition(value);
					},
					() =>
					{
						SetText(string.Empty);
					}
				));
			}

			Visible = false;
		}

		#endregion

		#region Private methods

		void SetText(string text)
		{
			if (descriptionText != null)
			{
				descriptionText.text = text;
			}
		}

		void SetVerticalPosition(float value)
		{
			rectTransform.anchoredPosition = new Vector2(
				rectTransform.anchoredPosition.x,
				value
			);
		}

		IEnumerator CO_Autohide()
		{
			yield return new WaitForSeconds(autohideDelay);

			// Call subscribed event handlers
			if (onAutoHide != null)
			{
				onAutoHide();
			}

			Hide(false);
		}

		#endregion
	}
}