using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UICaptionPanel : GameBehaviour
	{
		#region Fields

		public Text captionText;
		public Text subcaptionText;
		public float showHideDuration = 0.25f;
		public bool autohide = true;
		public float autohideDelay = 5f;

		RectTransform rectTransform;

		#endregion

		#region Properties

		public bool Visible
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			rectTransform = transform as RectTransform;
			Hide(true); // invisible by default
		}

		void OnDestroy()
		{
			StopAllCoroutines();
		}

		#endregion

		#region Public methods

		public void Show(string text, string subtext, bool immediate = false)
		{
			StopAllCoroutines();
			SetCaptionText(text);
			SetSubcaptionText(subtext);
			float targetPosition = 0;

			if (immediate)
			{
				SetHorizontalPosition(targetPosition);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					rectTransform.anchoredPosition.x,
					targetPosition,
					showHideDuration,
					null,
					(value) =>
					{
						SetHorizontalPosition(value);
					},
					() =>
					{
						if (autohide)
						{
							StartCoroutine(CO_Autohide());
						}
					}
				));
			}

			Visible = true;
		}

		public void Hide(bool immediate = false)
		{
			StopAllCoroutines();
			float targetPosition = rectTransform.rect.width;

			if (immediate)
			{
				SetHorizontalPosition(targetPosition);
				SetCaptionText(string.Empty);
				SetSubcaptionText(string.Empty);
			}
			else
			{
				StartCoroutine(CoroutineUtils.CO_LerpFloat(
					rectTransform.anchoredPosition.x,
					targetPosition,
					showHideDuration,
					null,
					(value) =>
					{
						SetHorizontalPosition(value);
					},
					() =>
					{
						SetCaptionText(string.Empty);
						SetSubcaptionText(string.Empty);
					}
				));
			}

			Visible = false;
		}

		public void SetAutohide(bool autohide)
		{
			this.autohide = autohide;
		}

		public void SetTextGradientColors(TextGradient.GradientColors gradientColors)
		{
			if (captionText != null)
			{
				TextGradient textGradient = captionText.GetComponent<TextGradient>();
				if (textGradient != null)
				{
					textGradient.gradientColors = gradientColors;
				}
			}
		}

		#endregion

		#region Private methods

		void SetCaptionText(string text)
		{
			if (captionText != null)
			{
				captionText.text = text;
			}
		}

		void SetSubcaptionText(string text)
		{
			if (subcaptionText != null)
			{
				subcaptionText.text = text;
			}
		}

		void SetHorizontalPosition(float value)
		{
			rectTransform.anchoredPosition = new Vector2(
				value,
				rectTransform.anchoredPosition.y
			);
		}

		IEnumerator CO_Autohide()
		{
			yield return new WaitForSeconds(autohideDelay);
			Hide(false);
		}

		#endregion

		#region Event handlers

		public void OnPointerDown()
		{
			if (Visible)
			{
				if (GameManager.Instance.GameState == GameState.Stopped)
				{
					// What is the current play result?
					switch (GameManager.Instance.PlayResult)
					{
						case PlayResult.Failed:
							AudioManager.Instance.PlaySound(Constants.Audio.Click);
							GameManager.Instance.RestartLevel();
							break;
						case PlayResult.Success:
							AudioManager.Instance.PlaySound(Constants.Audio.Click);
							if (LevelManager.Instance.IsFinalLevel)
							{
								GameManager.Instance.TitleScreen();
							}
							else
							{
								GameManager.Instance.NextLevel();
							}
							break;
					}
				}
				else if (GameManager.Instance.GameState == GameState.LevelLoaded)
				{
					AudioManager.Instance.PlaySound(Constants.Audio.Click);
					GameManager.Instance.Play();
				}
			}
		}

		#endregion
	}
}