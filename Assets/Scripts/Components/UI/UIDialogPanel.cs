using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Tilt
{
	public class UIDialogPanel : GameBehaviour
	{
		#region Fields

		public Text dialogText;
		public HorizontalLayoutGroup buttonsGroup;
		public Button buttonPrefab;

		Action<int> callback;
		List<Button> buttons = new List<Button>();

		#endregion

		#region Properties

		public bool Visible
		{
			get { return gameObject.activeInHierarchy; }
		}

		#endregion

		#region Public methods

		public void Show(string text, string[] buttonCaptions, Action<int> callback)
		{
			SetDialogText(text);
			InstantiateButtons(buttonCaptions);
			this.callback = callback;
			GameManager.Instance.Pause();
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			SetDialogText(string.Empty);
			DestroyButtons();
			callback = null;
			GameManager.Instance.Resume();
			PlayerManager.Instance.ResetInput();
			gameObject.SetActive(false);
		}

		#endregion

		#region Private methods

		void SetDialogText(string text)
		{
			if (dialogText != null)
			{
				dialogText.text = text;
			}
		}

		void InstantiateButtons(string[] buttonCaptions)
		{
			DestroyButtons();
			if (buttonPrefab != null && buttonsGroup != null)
			{
				buttonPrefab.gameObject.SetActive(false);
				for (int i = 0; i < buttonCaptions.Length; i++)
				{
					int index = i;
					Button button = Instantiate(buttonPrefab);
					button.onClick.AddListener(delegate { OnButtonClick(index); });
					button.GetComponent<Text>().text = buttonCaptions[i];
					button.gameObject.SetActive(true);
					button.transform.SetParent(buttonsGroup.transform, false);
					buttons.Add(button);
				}
			}
		}

		private void DestroyButtons()
		{
			for (int i = 0; i < buttons.Count; i++)
			{
				Button button = buttons[i];
				Destroy(button.gameObject);
				button = null;
			}
			buttons.Clear();
		}

		#endregion

		#region Event handlers

		void OnButtonClick(int index)
		{
			if (callback != null)
			{
				callback(index);
			}
			AudioManager.Instance.PlaySound(Constants.Audio.Click);
			Hide();
		}

		#endregion
	}
}
