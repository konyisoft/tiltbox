using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Dice : Rolling
	{
		#region Properties

		public int Number
		{
			get; private set;
		}

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			Number = 1;
		}

		#endregion

		#region Public methods

		public override void DoMovement(Direction direction)
		{
			if (nextMovement == NextMovement.Moving)
			{
				Vector3 axis = GetRotationAxisOfDirection(direction);
				Quaternion nextRotation = Quaternion.AngleAxis(90f, axis) * transform.localRotation;
				Number = CalculateNumberByRotation(nextRotation);
			}
			base.DoMovement(direction);
		}

		public void RecalculateNumber()
		{
			Number = CalculateNumberByRotation(transform.localRotation);
		}

		#endregion

		#region Private methods

		int CalculateNumberByRotation(Quaternion rotation)
		{
			int result = 0;
			if (VectorUtils.RoundToDecimals(rotation * Vector3.up, 0) == Vector3.up)
				result = 1;
			else if (VectorUtils.RoundToDecimals(rotation * Vector3.down, 0) == Vector3.up)
				result = 6;
			else if (VectorUtils.RoundToDecimals(rotation * Vector3.right, 0) == Vector3.up)
				result = 3;
			else if (VectorUtils.RoundToDecimals(rotation * Vector3.left, 0) == Vector3.up)
				result = 4;
			else if (VectorUtils.RoundToDecimals(rotation * Vector3.forward, 0) == Vector3.up)
				result = 2;
			else if (VectorUtils.RoundToDecimals(rotation * Vector3.back, 0) == Vector3.up)
				result = 5;
			//Debug.Log(result);
			return result;
		}

		#endregion
	}
}