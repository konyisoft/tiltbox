using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Sliding : Movable
	{
		#region Protected methods

		protected override void Move(Direction direction, OnAfterMove action = null)
		{
			float objectSize = LevelManager.Instance.objectSize;
			Vector3 targetPosition = transform.localPosition; // default

			switch (direction)
			{
				case Direction.Forward:
					targetPosition += new Vector3(0, 0, objectSize);
					Row++;
					break;

				case Direction.Back:
					targetPosition += new Vector3(0, 0, -objectSize);
					Row--;
					break;

				case Direction.Left:
					targetPosition += new Vector3(-objectSize, 0, 0);
					Col--;
					break;

				case Direction.Right:
					targetPosition += new Vector3(objectSize, 0, 0);
					Col++;
					break;
			}
			StartCoroutine(CO_Translate(targetPosition, PlayerManager.Instance.actionTime, direction, action));
		}

		#endregion

		#region Private methods

		IEnumerator CO_Translate(Vector3 targetPosition, float duration,
			Direction direction, OnAfterMove action = null)
		{
			float startTime = Time.time;
			Vector3 position = transform.localPosition;

			while (Time.time - startTime < duration)
			{
				transform.localPosition = Vector3.Lerp(position, targetPosition, (Time.time - startTime) / duration);
				yield return null;
			}

			// Precise final value
			transform.localPosition = VectorUtils.RoundToDecimals(targetPosition, 1);

			// Call action
			if (action != null)
			{
				action(direction);
			}

			yield return null;
		}

		#endregion
	}
}
