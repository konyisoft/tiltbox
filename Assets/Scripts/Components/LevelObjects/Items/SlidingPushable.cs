using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class SlidingPushable : Sliding
	{
		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			MovesOnlyWhenPushed = true;  // override
		}

		#endregion
	}
}
