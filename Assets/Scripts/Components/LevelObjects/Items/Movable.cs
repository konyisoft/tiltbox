using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	/// <summary>
	/// Abstract base class for movable (rolling, sliding, etc.) items.
	/// </summary>
	public abstract class Movable : Item
	{
		#region Fields

		[Header("Movable Item Fields")]
		public AllowedDirections allowedDirections = new AllowedDirections();
		public float fallingSpeed = 12f;
		public float fallingRotationSpeed = 200f;
		public float fallingDuration = 5f;
		public GameObject destroyingParticle;

		protected NextMovement nextMovement;
		protected Teleport teleportTarget;

		#endregion

		#region Fields

		public bool CanCollectItems
		{
			get; set;
		}

		public bool CanDie
		{
			get; set;
		}

		public bool MovesOnlyWhenPushed
		{
			get; set;
		}

		#endregion

		#region Delegates

		protected delegate void OnAfterMove(Direction direction);

		#endregion

		#region Abstract methods

		protected abstract void Move(Direction direction, OnAfterMove action = null);

		#endregion

		#region Public methods

		public virtual MovementResult PrepareNextMovement(Direction direction)
		{
			this.nextMovement = CalcNextMovement(direction);
			switch (nextMovement)
			{
				case NextMovement.Moving:
				case NextMovement.Teleporting:
					return MovementResult.Moved;

				case NextMovement.FallingOff:
				case NextMovement.FallingOffInPlace:
				case NextMovement.Die:
					return MovementResult.Failed;

				default:
					return MovementResult.None;
			}
		}

		public virtual NextMovement CalcNextMovement(Direction direction)
		{
			// NOTE: Conditions' order is very important here!
			// LevelObjects to check: current tile, neighbour tile, opposite item, neighbour item

			// Direction blocked by the item itself?
			if (allowedDirections.IsBlocked(direction))
			{
				return NextMovement.Blocked;
			}

			Tile currentTile = Level.GetTileBelow(this);

			if (currentTile != null)
			{
				// Direction blocked by the current tile? Don't move.
				if (currentTile.allowedDirections.IsBlocked(direction))
				{
					return NextMovement.Blocked;
				}

				// Able to move only when being pushed by the opposite neighbour item
				if (MovesOnlyWhenPushed)
				{
					Item oppositeItem = Level.GetNeighbourItemOf(this, LevelManager.GetOppositeDirection(direction));
					// Only one item can be pushed by another item. More can cause recursion.
					if (oppositeItem is Movable && !(oppositeItem as Movable).MovesOnlyWhenPushed)
					{
						// Do nothing here, just check the following conditions
					}
					else
					{
						return NextMovement.Blocked;
					}
				}

				// Checking neighbour tile
				Tile neighbourTile = Level.GetNeighbourTileOf(currentTile, direction);

				// Item may fall off?
				if (neighbourTile == null || neighbourTile.HasDestroyed ||  neighbourTile.CanFallThrough)
				{
					return NextMovement.FallingOff;
				}

				// Is it a teleport tile?
				if (neighbourTile is Teleport && (neighbourTile as Teleport).IsTargetClear)
				{
					teleportTarget = (neighbourTile as Teleport).Target;
					return NextMovement.Teleporting;
				}

				// Maybe deadly (and the item could die)?
				if (neighbourTile.IsDeadly && CanDie)
				{
					return NextMovement.Die;
				}

				// Neighbour tile blocks current direction?
				if (neighbourTile.allowedDirections.IsBlocked(direction))
				{
					// Fall through current tile
					if (currentTile.CanFallThrough)
					{
						return NextMovement.FallingOffInPlace;
					}
					else
					{
						return NextMovement.Blocked;
					}
				}

				// Checking neighbour item
				Item neighbourItem = Level.GetNeighbourItemOf(this, direction);

				// No neighbour or it is collectible
				if (neighbourItem == null || neighbourItem is Collectible)
				{
					return NextMovement.Moving;
				}

				// Neighbour is movable and going to move too.
				if (neighbourItem is Movable && (neighbourItem as Movable).CalcNextMovement(direction) != NextMovement.Blocked)
				{
					return NextMovement.Moving;
				}

				// Falling through the current tile
				if (currentTile.CanFallThrough)
				{
					return NextMovement.FallingOffInPlace;
				}
			}

			return NextMovement.Blocked; // default
		}

		public virtual void DoMovement(Direction direction)
		{
			switch (nextMovement)
			{
				case NextMovement.Blocked:
					// Do nothing
					break;

				case NextMovement.Moving:
					Move(direction, null);
					break;

				case NextMovement.Teleporting:
					Move(direction, Teleport);
					break;

				case NextMovement.FallingOff:
					Move(direction, FallOff);
					break;

				case NextMovement.FallingOffInPlace:
					FallOff(Direction.None); // No oOve here
					break;

				case NextMovement.Die:
					Move(direction, Die);
					break;
			}
			nextMovement = NextMovement.Blocked; // back to default
		}

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			nextMovement = NextMovement.Blocked;
			CanCollectItems = false;
			CanDie = false;
			MovesOnlyWhenPushed = false;
			teleportTarget = null;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			teleportTarget = null;
		}

		protected virtual void Teleport(Direction direction)
		{
			if (teleportTarget != null)
			{
				Level.RelocateItem(this, teleportTarget.Col, teleportTarget.Row);
				Vector3 oldPosition = transform.localPosition;
				Vector3 newPosition = teleportTarget.transform.localPosition;
				newPosition.y = transform.localPosition.y;
				transform.localPosition = newPosition;
				// TODO: displaying particle at old position
			}

		}

		protected virtual void Die(Direction direction)
		{
			Level.RemoveItemFromList(this);

			// Displaying a particle
			if (destroyingParticle != null)
			{
				GameObject particleObject = Instantiate(destroyingParticle);
				if (particleObject != null)
				{
					ParticleSystem particle = particleObject.GetComponent<ParticleSystem>();
					if (particle != null)
					{
						TemporaryManager.Instance.Add(particle);
						particle.transform.position = transform.position;
					}
				}
			}
			ReturnToPool();
		}

		protected virtual void FallOff(Direction direction)
		{
			StartCoroutine(CO_FallOff(GetRotationAxisOfDirection(direction), fallingDuration));
		}

		protected Vector3 GetRotationAxisOfDirection(Direction direction)
		{
			Vector3 axis = Vector3.forward; // default
			switch (direction)
			{
				case Direction.Forward:
					axis = Vector3.right;
					break;
				case Direction.Back:
					axis = Vector3.left;
					break;
				case Direction.Left:
					axis = Vector3.forward;
					break;
				case Direction.Right:
					axis = Vector3.back;
					break;
			}
			return axis;
		}

		#endregion

		#region Private methods

		IEnumerator CO_FallOff(Vector3 axis, float duration)
		{
			float startTime = Time.time;
			while (Time.time - startTime < duration)
			{
				transform.Translate(Vector3.down * Time.deltaTime * fallingSpeed, Space.World);
				transform.Rotate(axis * Time.deltaTime * fallingRotationSpeed, Space.World);
				yield return null;
			}
		}

		#endregion
	}
}
