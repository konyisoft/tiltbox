using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class Rolling : Movable
	{
		#region Protected methods

		protected override void Move(Direction direction, OnAfterMove action = null)
		{
			float halfObjectSize = LevelManager.Instance.objectSize * 0.5f;
			Vector3 pivot = transform.localPosition; // default
			Vector3 axis = GetRotationAxisOfDirection(direction);

			switch (direction)
			{
				case Direction.Forward:
					pivot += new Vector3(0, -halfObjectSize, halfObjectSize);
					Row++;
					break;

				case Direction.Back:
					pivot += new Vector3(0, -halfObjectSize, -halfObjectSize);
					Row--;
					break;

				case Direction.Left:
					pivot += new Vector3(-halfObjectSize, -halfObjectSize, 0);
					Col--;
					break;

				case Direction.Right:
					pivot += new Vector3(halfObjectSize, -halfObjectSize, 0);
					Col++;
					break;
			}
			StartCoroutine(CO_RotateAround(pivot, axis, 90f, PlayerManager.Instance.actionTime, direction, action));
		}

		#endregion

		#region Private methods

		IEnumerator CO_RotateAround(Vector3 pivot, Vector3 axis, float angle, float duration,
			Direction direction, OnAfterMove action = null)
		{
			float startTime = Time.time;
			Vector3 position = transform.localPosition;
			Quaternion rotation = transform.localRotation;

			while (Time.time - startTime < duration)
			{
				float currentAngle = Mathf.Lerp(0, angle, (Time.time - startTime) / duration);
				// Rotation
				transform.localRotation = Quaternion.AngleAxis(currentAngle, axis) * rotation;
				// Position
				transform.localPosition = (Quaternion.Euler(axis * currentAngle) * (position - pivot)) + pivot;
				yield return null;
			}

			// Precise final values
			transform.localRotation = Quaternion.AngleAxis(angle, axis) * rotation;
			transform.localPosition = VectorUtils.RoundToDecimals(
				(Quaternion.Euler(axis * angle) * (position - pivot)) + pivot,
				1
			);

			// Call action
			if (action != null)
			{
				action(direction);
			}

			yield return null;
		}

		#endregion
	}
}
