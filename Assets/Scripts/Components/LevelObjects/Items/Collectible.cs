using System;
using UnityEngine;

namespace Konyisoft.Tilt
{
	/// <summary>
	/// An item that can be collected by other items (and disappears after touched).
	/// Similar to the vanishing tile.
	/// </summary>
	public class Collectible : Item
	{
		#region Fields

		public GameObject particlePrefab;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			Priority = 1;
		}

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			Level.onBeforeSetNextMovement += OnBeforeSetNextMovement;
			Level.onAfterMoveItems += OnAfterMoveItems;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			Level.onBeforeSetNextMovement -= OnBeforeSetNextMovement;
			Level.onAfterMoveItems -= OnAfterMoveItems;
		}

		#endregion

		#region Private methods

		void Collected()
		{
			Level.RemoveItemFromList(this);
			// Displaying a particle
			if (particlePrefab != null)
			{
				GameObject particleObject = Instantiate(particlePrefab);
				if (particleObject != null)
				{
					ParticleSystem particle = particleObject.GetComponent<ParticleSystem>();
					if (particle != null)
					{
						TemporaryManager.Instance.Add(particle);
						particle.transform.position = transform.position;
					}
				}
			}
			ReturnToPool(); // do not destroy, return to the pool
		}

		#endregion

		#region Event handlers

		void OnBeforeSetNextMovement(Direction direction)
		{
			// A neighbouring movable item will collect _this_ item?
			Direction oppositeDirection = LevelManager.GetOppositeDirection(direction);
			Item neighbourItem = Level.GetNeighbourItemOf(this, oppositeDirection);
			if (neighbourItem is Movable && (neighbourItem as Movable).CanCollectItems)
			{
				HasDestroyed = true; // mark as destroyed
			}
		}

		void OnAfterMoveItems(MovementResult movementResult)
		{
			// Collected after all items moved
			if (HasDestroyed)
			{
				Collected();
			}
		}

		#endregion
	}
}
