using UnityEngine;

namespace Konyisoft.Tilt
{
	/// <summary>
	/// Not a good implementation. Does not handle contacts with other
	/// moving items. Use this with carefull.
	/// </summary>
	public class RollingReversed : Rolling
	{
		#region Protected methods
		
		protected override void Move(Direction direction, OnAfterMove action = null)
		{
			base.Move(ReverseDirection(direction), action);
		}
		
		protected override void FallOff(Direction direction)
		{
			base.FallOff(ReverseDirection(direction));
		}		

		#endregion

		#region Protected methods

		public override NextMovement CalcNextMovement(Direction direction)
		{
			// Calculations are also reversed
			return base.CalcNextMovement(ReverseDirection(direction));
		}

		#endregion

		#region Private methods

		Direction ReverseDirection(Direction direction)
		{
			Direction reversed = Direction.None;
			switch (direction)
			{
				case Direction.Forward:
					reversed = Direction.Back;
				break;
				case Direction.Back:
					reversed = Direction.Forward;
				break;
				case Direction.Left:
					reversed = Direction.Right;
				break;
				case Direction.Right:
					reversed = Direction.Left;
				break;
			}
			return reversed;
		}

		#endregion
	}
}