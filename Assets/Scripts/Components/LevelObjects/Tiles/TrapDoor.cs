using System.Collections;
using UnityEngine;

namespace Konyisoft.Tilt
{
	public class TrapDoor : Tile
	{
		#region Enumerations

		public enum TrapDoorState
		{
			Opened,
			Closed
		}

		#endregion

		#region Fields

		public Vector3 axis = Vector3.forward;
		public Vector3 pivot = Vector3.zero;

		#endregion

		#region Properties

		public TrapDoorState State
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			State = TrapDoorState.Closed;
			CanFallThrough = false; // at start
		}

		#endregion

		#region Protected methods

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			State = TrapDoorState.Closed;
			CanFallThrough = false;
		}

		#endregion

		#region Public methods

		public void Open(bool immediate = false)
		{
			if (State == TrapDoorState.Opened)
				return;

			StopAllCoroutines();
			if (immediate)
			{
				RotateAroundImmediate(transform.localPosition + (transform.localRotation * pivot), axis, -90f);
			}
			else
			{
				StartCoroutine(CO_RotateAround(
					transform.localPosition + (transform.localRotation * pivot),
					axis,
					-90f,
					PlayerManager.Instance.actionTime * 0.65f
				));
			}
			State = TrapDoorState.Opened;
			CanFallThrough = true;
		}

		public void Close(bool immediate = false)
		{
			if (State == TrapDoorState.Closed)
				return;

			StopAllCoroutines();
			if (immediate)
			{
				RotateAroundImmediate(transform.localPosition + (transform.localRotation * pivot), axis, 90f);
			}
			else
			{
				StartCoroutine(CO_RotateAround(
					transform.localPosition + (transform.localRotation * pivot),
					axis,
					90f,
					PlayerManager.Instance.actionTime * 0.65f
				));
			}
			State = TrapDoorState.Closed;
			CanFallThrough = false;
		}

		public void Toggle(bool immediate = false)
		{
			switch (State)
			{
				case TrapDoorState.Opened:
					Close(immediate);
					break;
				case TrapDoorState.Closed:
					Open(immediate);
					break;
			}
		}

		#endregion

		#region Private methods

		void RotateAroundImmediate(Vector3 pivot, Vector3 axis, float angle)
		{
			transform.localRotation = Quaternion.AngleAxis(angle, axis) * transform.localRotation;
			transform.localPosition = VectorUtils.RoundToDecimals(
				(Quaternion.Euler(axis * angle) * (transform.localPosition - pivot)) + pivot,
				1
			);
		}

		IEnumerator CO_RotateAround(Vector3 pivot, Vector3 axis, float angle, float duration)
		{
			float startTime = Time.time;
			Vector3 position = transform.localPosition;
			Quaternion rotation = transform.localRotation;

			while (Time.time - startTime < duration)
			{
				float currentAngle = Mathf.Lerp(0, angle, (Time.time - startTime) / duration);
				// Rotation
				transform.localRotation = Quaternion.AngleAxis(currentAngle, axis) * rotation;
				// Position
				transform.localPosition = (Quaternion.Euler(axis * currentAngle) * (position - pivot)) + pivot;
				yield return null;
			}

			// Precise final values
			transform.localRotation = Quaternion.AngleAxis(angle, axis) * rotation;
			transform.localPosition = VectorUtils.RoundToDecimals(
				(Quaternion.Euler(axis * angle) * (position - pivot)) + pivot,
				1
			);
			yield return null;
		}

		#endregion
	}
}
