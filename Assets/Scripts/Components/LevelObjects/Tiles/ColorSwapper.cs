using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Tilt
{
	/// <summary>
	/// A tile that changes its color when touched.
	/// </summary>
	public class ColorSwapper : Tile
	{
		#region Fields

		Color32[] colors = new Color32[0];

		#endregion

		#region Properties

		public int ColorIndex
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			CopyMaterial(); // if needed
		}

		#endregion

		#region Public methods

		public void SetColors(Color32[] colors)
		{
			this.colors = colors;
		}

		public void SetStartColorIndex(int startColorIndex)
		{
			SetColorByIndex(startColorIndex);
		}

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			Level.onBeforeSetNextMovement += OnBeforeSetNextMovement;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			Level.onBeforeSetNextMovement -= OnBeforeSetNextMovement;

			// Reset internal values
			colors = new Color32[0];
		}

		#endregion

		#region Private methods

		void SetMaterialColor(Color32 color)
		{
			if (copyMaterial != null)
			{
				copyMaterial.color = color;
			}
		}

		void SetColorByIndex(int index)
		{
			if (index >= 0 && index < colors.Length)
			{
				SetMaterialColor(colors[index]);
				ColorIndex = index;
			}
		}

		void NextColor(bool immediate = false)
		{
			if (colors.Length == 0)
				return;

			if (ColorIndex == colors.Length - 1)
			{
				ColorIndex = 0;
			}
			else
			{
				ColorIndex++;
			}

			if (immediate)
			{
				SetColorByIndex(ColorIndex);
			}
			else
			{
				StartCoroutine(CO_SetColorByIndex(PlayerManager.Instance.actionTime * 0.5f, ColorIndex));
			}
		}

		#endregion

		#region Event handlers

		void OnBeforeSetNextMovement(Direction direction)
		{
			// A neighbouring movable item _will_ touch _this_ tile?
			Direction oppositeDirection = LevelManager.GetOppositeDirection(direction);
			Tile neighbourTile = Level.GetNeighbourTileOf(this, oppositeDirection);
			if (neighbourTile != null && neighbourTile.allowedDirections.IsAllowed(direction))
			{
				Item item = Level.GetItemAbove(neighbourTile);
				if (item is Movable)
				{
					NextColor(); // rotating colors
				}
			}
		}

		IEnumerator CO_SetColorByIndex(float waitBefore, int index)
		{
			yield return new WaitForSeconds(waitBefore);
			SetColorByIndex(index);
		}

		#endregion
	}
}
