using System;

namespace Konyisoft.Tilt
{
	public class Teleport : Tile
	{
		#region Properties

		public Teleport Target
		{
			get; private set;
		}
		
		public bool HasTarget
		{
			get { return Target != null; }
		}

		public bool IsTargetClear
		{
			get { return HasTarget && Level.GetItemAbove(Target) == null; }
		}

		#endregion

		#region Protected methods

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			Disconnect(); // Remove connection
		}

		#endregion

		#region Public methods

		public void Connect(Teleport target)
		{
			Target = target;
		}

		public void Disconnect()
		{
			Target = null;
		}

		#endregion
	}
}
