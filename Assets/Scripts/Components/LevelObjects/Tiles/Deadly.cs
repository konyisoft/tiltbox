namespace Konyisoft.Tilt
{
	public class Deadly : Tile
	{
		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			IsDeadly = true;
		}

		#endregion
	}
}
