using System;
using UnityEngine;

namespace Konyisoft.Tilt
{
	/// <summary>
	/// A tile that can be touched only once, thereafter immediately vanishes.
	/// </summary>
	public class Vanishing : Tile
	{
		#region Fields

		public GameObject particlePrefab;

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			Level.onBeforeSetNextMovement += OnBeforeSetNextMovement;
			Level.onAfterMoveItems += OnAfterMoveItems;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			Level.onBeforeSetNextMovement -= OnBeforeSetNextMovement;
			Level.onAfterMoveItems -= OnAfterMoveItems;
		}

		#endregion

		#region Private methods

		void Vanish()
		{
			Level.RemoveTileFromList(this);
			// Displaying a particle
			if (particlePrefab != null)
			{
				GameObject particleObject = Instantiate(particlePrefab);
				if (particleObject != null)
				{
					ParticleSystem particle = particleObject.GetComponent<ParticleSystem>();
					if (particle != null)
					{
						TemporaryManager.Instance.Add(particle);
						particle.transform.position = transform.position;
					}
				}
			}
			ReturnToPool(); // do not destroy, return to the pool
		}

		#endregion

		#region Event handlers

		void OnBeforeSetNextMovement(Direction direction)
		{
			Item item = Level.GetItemAbove(this);
			if (item is Movable && (item as Movable).CalcNextMovement(direction) != NextMovement.Blocked)
			{
				HasDestroyed = true; // mark as destroyed
			}
		}

		void OnAfterMoveItems(MovementResult movementResult)
		{
			// Vanish after all items moved
			if (HasDestroyed)
			{
				Vanish();
			}
		}

		#endregion
	}
}
