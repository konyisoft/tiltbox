namespace Konyisoft.Tilt
{
	/// <summary>
	/// A tile which works as a two state button.
	/// </summary>
	public class Switcher : Tile
	{
		#region Enumerations

		public enum SwitchState
		{
			Off,
			On
		}

		#endregion

		#region Events

		public delegate void OnStateChange(SwitchState state);
		public event OnStateChange onStateChange;

		#endregion

		#region Properties

		public SwitchState State
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			State = SwitchState.Off;
		}

		#endregion

		#region Protected methods

		protected override void OnInitialized()
		{
			base.OnInitialized();
			Level.onBeforeSetNextMovement += OnBeforeSetNextMovement;
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			Level.onBeforeSetNextMovement -= OnBeforeSetNextMovement;
			State = SwitchState.Off;
		}

		#endregion

		#region Private methods

		void Off()
		{
			if (State != SwitchState.Off)
			{
				State = SwitchState.Off;
				if (onStateChange != null)
				{
					onStateChange(State);
				}
			}
		}

		void On()
		{
			if (State != SwitchState.On)
			{
				State = SwitchState.On;
				if (onStateChange != null)
				{
					onStateChange(State);
				}
			}
		}

		void OnBeforeSetNextMovement(Direction direction)
		{
			// A neighbouring movable item _will_ touch _this_ tile?
			Direction oppositeDirection = LevelManager.GetOppositeDirection(direction);
			Tile neighbourTile = Level.GetNeighbourTileOf(this, oppositeDirection);
			if (neighbourTile != null && neighbourTile.allowedDirections.IsAllowed(direction))
			{
				// Touched by a movable item? Turn on.
				Item item = Level.GetItemAbove(neighbourTile);
				if (item != null && (item is Movable))
				{
					On();
				}
				// Otherwise turn it off
				else
				{
					Off();
				}
			}
		}

		#endregion	
	}
}
