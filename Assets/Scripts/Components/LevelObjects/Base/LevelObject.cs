using UnityEngine;

namespace Konyisoft.Tilt
{
    /// <summary>
    /// Base class for all level objects (tiles and items).
    /// </summary>
    public class LevelObject : PooledObject
	{
		#region Fields

		[Header("LevelObject Fields")]
		public bool randomizeTexture;
		public int textureTiling = 4;

		protected Material copyMaterial;
		protected Vector3 origPosition;
		protected Quaternion origRotation;

		#endregion		
		
		#region Properties

		public int Col
		{
			get; protected set;
		}

		public int Row
		{
			get; protected set;
		}

		public Level Level
		{
			get; protected set;
		}

		public string Name
		{
			get { return gameObject.name; }
		}

		public bool HasDestroyed
		{
			get; protected set;
		}

		public int Priority
		{
			get; protected set;
		}

		#endregion

		#region Mono methods

		protected virtual void Awake()
		{
			origPosition = transform.localPosition;
			origRotation = transform.localRotation;

			if (randomizeTexture && textureTiling > 0)
			{
				CopyMaterial();
				if (copyMaterial != null)
				{
					RandomizeTexture();
				}
			}
		}

		protected virtual void OnDestroy()
		{
			StopAllCoroutines();
			if (copyMaterial != null)
			{
				Destroy(copyMaterial);
			}
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			// Restore original position and rotation after returning to the pool
			transform.localPosition = origPosition;
			transform.localRotation = origRotation;
		}

		#endregion

		#region Public methods

		public void Initialize(Level level, int col, int row)
		{
			SetLevel(level);
			SetCoords(col, row);
			HasDestroyed = false;
			OnInitialized();
		}

		internal void SetCoords(int col, int row)
		{
			this.Col = col;
			this.Row = row;
		}

		internal void SetLevel(Level level)
		{
			this.Level = level;
		}

		#endregion

		#region Protected methods

		protected void CopyMaterial()
		{
			if (copyMaterial == null)
			{
				Renderer renderer = GetComponent<Renderer>();
				if (renderer != null && renderer.material != null)
				{
					copyMaterial = new Material(renderer.material);
					renderer.material = copyMaterial;
				}	
			}
		}

		#endregion

		#region Private methods

		void RandomizeTexture()
		{
			float multiplier = 1f / (float)textureTiling;
			float x = Random.Range(0, textureTiling) * multiplier;
			float y = Random.Range(0, textureTiling) * multiplier;
			copyMaterial.mainTextureOffset = new Vector2(x, y);
		}

		#endregion
	}
}
