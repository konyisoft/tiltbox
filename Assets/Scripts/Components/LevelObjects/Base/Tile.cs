namespace Konyisoft.Tilt
{
	/// <summary>
	/// Base class for level tiles (ground objects).
	/// </summary>
	public class Tile : LevelObject
	{
		#region Fields

		public AllowedDirections allowedDirections = new AllowedDirections();

		#endregion

		#region Properties

		public bool CanFallThrough
		{
			get; set;
		}

		public bool IsDeadly
		{
			get; set;
		}

		#endregion
	}
}
