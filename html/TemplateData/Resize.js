(function() {
  initialize();

  function initialize() {
    window.addEventListener('resize', resizeCanvas, false);
    resizeCanvas();
  }

  function resizeCanvas() {
    var canvas = document.getElementById('#canvas');
    if (canvas != null) {
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;
    }
  }
})();
