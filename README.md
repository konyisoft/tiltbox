# TiltBox
**by Konyisoft**

TiltBox is a simple yet challenging puzzle game in which you have to solve different tasks by tilting the level.

Levels have various problems to solve:
- moving crystals to the proper places
- pushing items to the proper places
- destroying tiles
- changing color of tiles
- collecting items
- avoiding deadly floor and trapdoors
- not to fall off the edges
- ...and many other things.

Any of the levels can be played from the start, no need to solve them in order. You can disable the time limit in the settings menu to make things easier, but in this mode the level results will not be saved.

At this time there are **22** levels to play with.

![Screenshot](tiltbox.jpg)

[TiltBox on Google Play](https://play.google.com/store/apps/details?id=com.konyisoft.tiltbox)  
[WebGL, Windows and Linux versions on Game Jolt](https://gamejolt.com/games/tiltbox/355507)

Created with Unity3D 2017.4 on Ubuntu.